using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Date : UnityEngine.Object, IEquatable<Date>, IComparable<Date>
{
    public int Year { get; }
    public int Month { get; }
    public int Day { get; }
    public int Hour { get; }
    public int Minute { get; }
    public int Second { get; }
    private long seconds;

    public const int secondsPerDay = 24 * 3600;
    public const int daysPerYear = 365;
    public const int secondsPerYear = secondsPerDay * daysPerYear;
    public static int[] daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    public static int[] monthShift;

    public static  int referenceYear = 1970;    
 
    public Date(long timeInSeconds)
    {
        int dayInYear = (int)(timeInSeconds % secondsPerYear / secondsPerDay);
        int timeOfDay = (int)(timeInSeconds % secondsPerDay);
        Second = timeOfDay % 60;
        Minute = timeOfDay % 3600 / 60;
        Hour = timeOfDay / 3600;
        Month = MonthFromSeconds(timeInSeconds);
        Day = dayInYear - monthShift[Month - 1] + 1;
        Year = referenceYear + (int)(timeInSeconds / secondsPerYear);
        seconds = timeInSeconds;

        // with leap year
        //year = t0 + dayNumber / 31536000 % 31536000 / 31622400;
    }
    public Date(int year, int month, int day, int hour, int minute, int second)
    {
        this.Year = year;
        this.Month = month;
        this.Day = day;
        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
        seconds = DateToSeconds(year, month, day, hour, minute, second);
    }
    public override int GetHashCode()
    {
        return seconds.GetHashCode();
    }
    public bool Equals(Date other)
    {
        return seconds == other.seconds;
    }
    public override bool Equals(System.Object obj)
    {
        if (obj == null || ((Date) obj) != null)
            return false;
        return Equals(obj as Date);
    }
    public int CompareTo(Date other)
    {
        return seconds.CompareTo(other.seconds);
    }
    public long ToSeconds()
    {
        return seconds;
        //return (Year - referenceYear) * secondsPerYear + MonthToSeconds(Month) + ((Day - 1) * 24 + Hour) * 3600 + Minute * 60 + Second; 
    }
    public static long DateToSeconds(int year, int month, int day, int hour, int minute, int second)
    {
        if (monthShift == null)
            ComputeMonthShift();
        return (year - referenceYear) * secondsPerYear + MonthToSeconds(month) +
            ((day - 1) * 24 + hour) * 3600 + minute * 60 + second;
    }
    public override string ToString()
    {    
        return string.Format("{0}.{1}.{2} {3}:{4}:{5}", Year, Month, Day, Hour, Minute, Second);
    }
    public static int MonthFromSeconds (long seconds)
    {
        if (monthShift == null) 
            ComputeMonthShift();
        int dayInYear = (int)(seconds % secondsPerYear / secondsPerDay);
        int month;
        for (month = 0; month < 12; month++)
        {
            if (dayInYear < monthShift[month]) break;
        }
        return month;
    }
    public static long MonthToSeconds(int month)
    {
        if (monthShift == null)
            ComputeMonthShift();
        return monthShift[month - 1] * 3600 * 24;
    }
    private static void ComputeMonthShift()
    {
        monthShift = new int[daysInMonth.Length];
        
        monthShift[0] = 0;
        for (int i = 1; i < monthShift.Length; i++)  
        {
            monthShift[i] = monthShift[i - 1] + daysInMonth[i - 1];
        }
    }
}