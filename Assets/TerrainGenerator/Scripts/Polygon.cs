using System;
using System.Collections;
using System.Collections.Generic;
using DelaunatorSharp;
using UnityEngine;

public class Triangle
{
    public Vector3[] Vertices;

    private Triangle(Vector3[] vertices)
    {
        Vertices = vertices;
    }
}

public class Polygon
{
    public Vector3[] Vertices { get; private set; }
    public int[] Indices { get; private set; }
    public Polygon(Vector3[] vertices, int[] indices)
    {
        Vertices = vertices;

    }

    // Triangulate convex polygon
    // Work with indecies of an array of vertices.
    public int[] Triangulate()
    {
        int n = Indices.Length;
        int[] triangles = new int[n - 2];
        triangles[0] = Indices[0];
        
        // Iterate over consecutive pairs of vertices
        int j = 1;
        for (int i = 1; i < n - 1; i++)
        {
            triangles[j++] = Indices[i];
            triangles[j++] = Indices[i + 1];
        }
        return triangles;   
    }


    // In Unity, not needed
    private static Vector3 CrossProduct(Vector3 a, Vector3 b)
    {
        float crossX = a.y * b.z - a.z * b.y;
        float crossY = a.z * b.x - a.x * b.z;
        float crossZ = a.x * b.y - a.y * b.x;

        return new Vector3(crossX, crossY, crossZ);
    }
    // In Unity, not needed

    public static Vector3 Normalize(Vector3 v)
    {
        float magnitude = Mathf.Sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
        return new Vector3(v.x / magnitude, v.y / magnitude, v.z / magnitude);
    }

    // Get the normal to a triangle from the three corner points a, b, and o, 
    // where o is the origin point of vectors a and b.
    public Vector3 GetNormal(Vector3 a, Vector3 b, Vector3 o)
    {
        Vector3 side1 = a - o;
        Vector3 side2 = b - o;
        return Vector3.Cross(side1, side2).normalized;
    }

    bool IsPolygonConvex(List<Vector3> vertices)
    {
        int count = vertices.Count;
        if (count < 3)
        {
            Debug.LogError("Polygon must have at least 3 vertices.");
            return false;
        }

        for (int i = 0; i < count; i++)
        {
            Vector3 prev = vertices[(i - 1 + count) % count];
            Vector3 current = vertices[i];
            Vector3 next = vertices[(i + 1) % count];

            Vector3 edge1 = prev - current;
            Vector3 edge2 = next - current;
            Vector3 normal = Vector3.Cross(edge1, edge2);

            for (int j = 0; j < count; j++)
            {
                if (j == i || j == (i - 1 + count) % count || j == (i + 1) % count)
                    continue;

                Vector3 testPoint = vertices[j];
                Vector3 testEdge = testPoint - current;
                float dotProduct = Vector3.Dot(normal, testEdge);

                if (dotProduct < 0)
                {
                    return false; // Polygon is not convex
                }
            }
        }

        return true; // Polygon is convex
    }
}
