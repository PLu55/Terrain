using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoronoiVertexElevationComparer : IComparer<VoronoiVertex>
{
    public int Compare(VoronoiVertex x, VoronoiVertex y)
    {
        // Compare based on the Value property
        return x.elevation.CompareTo(y.elevation);
    }
}

public class VoronoiVertex
{
    public int Index { get; } // Index into the array of vertices
    // public Vector3 point;
    public bool ocean;
    public bool water;
    public bool coast;
    public bool border;
    public float elevation;
    public float moisture;
    
    public List<VoronoiCell> touches;
    public List<VoronoiEdge> protrudes;
    public List<VoronoiVertex> adjacent;

    public int river;  // 0 if no river, or volume of water in river
    public VoronoiVertex downslope;  // pointer to adjacent corner most downhill
    public VoronoiVertex watershed;  // pointer to coastal corner, or null
    public int watershed_size;
    public VoronoiVertex(int index)
    {
        Index = index;
        touches = new List<VoronoiCell>();
        protrudes = new List<VoronoiEdge>();
        adjacent = new List<VoronoiVertex>();
    }
    override public string ToString()
    {
        return string.Format("V{0}", Index);
    }

    public Vector3 GetVertex()
    {
        return Graph.TheGraph.CellVertices[Index];
    }
}

