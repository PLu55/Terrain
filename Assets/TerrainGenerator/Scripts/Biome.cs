using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public enum BiomeRegion {Unknown, Polar, Subpolar, Boreal, Cool, Warm, Subtropical, Tropical, count};
public enum BiomeType {Unknown, Tundra, Forest, Desert, Stepp, Woodland, Thorn, Scrub, count};
public enum BiomeSubType {Unknown, None, Scrub, Thorn, VeryDry, Dry, Moist, Wet, Rain, count}
public enum BiomeHumidity {unknown, Semiparched, Superarid, Perarid, Arid, Semiarid, Subhumid, Humid, Perhumid, Superhumid, count};
public enum BiomeAltitude {unknown, Basal, Premontane, LowMontaine, Montaine, Subalpine, Alpine, Nival, count};

public class Biome
{
    public float precipitation;
    public float biotemprature;
    public float evapotranspiration;
    public BiomeRegion region;
    public BiomeType type;
    public BiomeSubType subType;
    public BiomeHumidity humidity;
    public BiomeAltitude altitude;

    public Biome(float precipitation, float biotemprature)
    {
        this.precipitation = precipitation;
        this.biotemprature = biotemprature;

        if (precipitation < 62.5f || precipitation > 16000.0f)
        {
            Debug.LogErrorFormat("Illegal percipitation: {0}", precipitation);
        }

        region = BiomeRegion.Unknown;
        type = BiomeType.Unknown;
        subType = BiomeSubType.Unknown;
        humidity = BiomeHumidity.unknown;
        altitude = BiomeAltitude.unknown;

        switch (biotemprature)
        {
            case < 1.5f:
                region = BiomeRegion.Polar;
                break;
            case < 3.0f:
                region = BiomeRegion.Subpolar;
                type = BiomeType.Tundra;
                switch (precipitation)
                {
                    case < 125.0f:
                        subType = BiomeSubType.Dry;
                        humidity = BiomeHumidity.Subhumid;
                        break;
                    case < 250.0f:
                        subType = BiomeSubType.Moist;
                        humidity = BiomeHumidity.Humid;
                        break;
                    case < 500.0f:
                        subType = BiomeSubType.Wet;
                        humidity = BiomeHumidity.Perhumid;
                        break;
                    case < 1000.0f:
                        subType = BiomeSubType.Rain;
                        humidity = BiomeHumidity.Superhumid;
                        break;
                    default:
                        Debug.LogWarningFormat("Illegal percipitation: {0} in region: {1}", precipitation, region); 
                        break;
                }
                break;
            case < 6.0f:
                region = BiomeRegion.Boreal;
                switch (precipitation)
                {
                    case < 125.0f:
                        type = BiomeType.Desert;
                        subType = BiomeSubType.None;
                        humidity = BiomeHumidity.Semiarid;
                        break;
                    case < 250.0f:
                        type = BiomeType.Scrub;
                        subType = BiomeSubType.Dry;
                        humidity = BiomeHumidity.Subhumid;
                        break;
                    case < 500.0f:
                        type = BiomeType.Forest;
                        subType = BiomeSubType.Moist;
                        humidity = BiomeHumidity.Humid;
                        break;
                    case < 1000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Wet;
                        humidity = BiomeHumidity.Perhumid;
                        break;
                    case < 2000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Rain;
                        humidity = BiomeHumidity.Superhumid;
                        break;
                    default:
                        Debug.LogErrorFormat("Illegal percipitation: {0} in region: {1}", precipitation, region);
                        break;
                }
                break;
            case < 12.0f:
                region = BiomeRegion.Cool;
                switch (precipitation)
                {
                    case < 125.0f:
                        type = BiomeType.Desert;
                        subType = BiomeSubType.None;
                        humidity = BiomeHumidity.Arid;
                        break;
                    case < 250.0f:
                        type = BiomeType.Desert;
                        subType = BiomeSubType.Scrub;
                        humidity = BiomeHumidity.Semiarid;
                        break;
                    case < 500.0f:
                        type = BiomeType.Stepp;
                        subType = BiomeSubType.None;
                        humidity = BiomeHumidity.Subhumid;
                        break;
                    case < 1000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Moist;
                        humidity = BiomeHumidity.Humid;
                        break;
                    case < 2000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Wet;
                        humidity = BiomeHumidity.Perhumid;
                        break;
                    case < 4000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Rain;
                        humidity = BiomeHumidity.Superhumid;
                        break;
                    default:
                        Debug.LogErrorFormat("Illegal percipitation: {0} in region: {1}", precipitation, region);
                        break;
                }
                break;
            case < 24.0f:
                region = BiomeRegion.Warm;
                switch (precipitation)
                {
                    case < 125.0f:
                        type = BiomeType.Desert;
                        subType = BiomeSubType.None;
                        humidity = BiomeHumidity.Perarid;
                        break;
                    case < 250.0f:
                        type = BiomeType.Desert;
                        subType = BiomeSubType.Scrub;
                        humidity = BiomeHumidity.Arid;
                        break;
                    case < 500.0f:
                        type = BiomeType.Stepp;
                        if (biotemprature < 16.97f)
                        {
                            subType = BiomeSubType.Thorn;
                        }
                        else
                        {
                            type = BiomeType.Woodland;
                            subType = BiomeSubType.Thorn; 
                        }
                        humidity = BiomeHumidity.Semiarid;
                        break;
                    case < 1000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Dry;
                        humidity = BiomeHumidity.Subhumid;
                        break;
                     case < 2000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Moist;
                        humidity = BiomeHumidity.Humid;
                        break;
                    case < 4000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Wet;
                        humidity = BiomeHumidity.Perhumid;
                        break;
                    case < 8000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Rain;
                        humidity = BiomeHumidity.Superhumid;
                        break;
                    default:
                        Debug.LogErrorFormat("Illegal percipitation: {0} in region: {1}", precipitation, region);
                        break;
                }
                break;
            default:
                region = BiomeRegion.Tropical;
                switch (precipitation)
                {
                    case < 125.0f:
                        type = BiomeType.Desert;
                        subType = BiomeSubType.None;
                        humidity = BiomeHumidity.Superarid;
                        break;
                    case < 250.0f:
                        type = BiomeType.Desert;
                        subType = BiomeSubType.Scrub;
                        humidity = BiomeHumidity.Perarid;
                        break;
                    case < 500.0f:
                        type = BiomeType.Woodland;
                        subType = BiomeSubType.Thorn;
                        humidity = BiomeHumidity.Arid;
                        break;
                    case < 1000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.VeryDry;
                        humidity = BiomeHumidity.Semiarid;
                        break;
                     case < 2000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Dry;
                        humidity = BiomeHumidity.Subhumid;
                        break;
                    case < 4000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Moist;
                        humidity = BiomeHumidity.Humid;
                        break;
                    case < 8000.0f:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Wet;
                        humidity = BiomeHumidity.Perhumid;
                        break;
                    default:
                        type = BiomeType.Forest ;
                        subType = BiomeSubType.Rain;
                        humidity = BiomeHumidity.Superhumid;
                        break;
                }
                break;
        }
        
        // value in the center
        switch (humidity)
        {
            case BiomeHumidity.unknown:
                evapotranspiration = -1.0f;
                break;
            case BiomeHumidity.Superhumid:
                evapotranspiration = 0.176776695296637f;
                break;
            case BiomeHumidity.Perhumid:
                evapotranspiration = 0.353553390593274f;
                break;
            case BiomeHumidity.Humid:
                evapotranspiration = 0.707106781186548f;
                break;
            case BiomeHumidity.Subhumid:
                evapotranspiration = 1.4142135623731f;
                break;
            case BiomeHumidity.Semiarid:
                evapotranspiration = 2.82842712474619f;
                break;
            case BiomeHumidity.Arid:
                evapotranspiration = 5.65685424949238f;
                break;
            case BiomeHumidity.Perarid:
                evapotranspiration = 11.3137084989848f;
                break;
            case BiomeHumidity.Superarid:
                evapotranspiration = 22.6274169979695f;
                break;
            case BiomeHumidity.Semiparched:
                evapotranspiration = 45.254833995939f;
                break;
        }
    }

    public override string ToString()
    {
        return string.Format("Biome({0},{1},{2})", region, type, subType);
    }
    public string ToStringShort()
    {
        return string.Format("({0},{1},{2})", region, type, subType);
    }
    public string Info()
    {
        return string.Format("Biome({0},{1}):\n ", type, subType) +
            string.Format("    region: {0}):\n ", region) +
            string.Format("    humidity: {0}):\n ", humidity) +
            string.Format("    altitude: {0}):\n ", altitude) +
            string.Format("    precipitation: {0}):\n ", precipitation) +
            string.Format("    biotemprature: {0}):\n ", biotemprature) +
            string.Format("    evapotranspiration: {0}):\n ", evapotranspiration);
    }
    public static int EncodeBiomeTypes(BiomeRegion region, BiomeType type, BiomeSubType subType)
    {
        return (int)region + (int)type * (int)BiomeRegion.count + (int)subType * (int)BiomeType.count * (int)BiomeRegion.count;
    }
    public static int[] DecodeBiomeTypes(int d)
    {
        int[] res = new int[3];
        int n0 = (int)BiomeRegion.count;
        int n01 = (int)BiomeRegion.count * (int)BiomeType.count;
        res[2] = d / n01;
        res[1] = d % n01 / n0;
        res[0] = d % n0;
        return res;
    }
}
