using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;

public class Climate : MonoBehaviour
{    
    public float[] PrecipitationDistributionPolar;
    public float[] PrecipitationDistributionSubpolar;
    public float[] PrecipitationDistributionBoreal;
    public float[] PrecipitationDistributionCool;
    public float[] PrecipitationDistributionWarm;
    public float[] PrecipitationDistributionSubtropical;
    public float[] PrecipitationDistributionTropical;

    static public Climate Instance { get; private set; }
    void Awake()
    {
        Instance = this;
    }
    void OnEnable ()
    {
        BalanceDistribution(PrecipitationDistributionPolar);
        BalanceDistribution(PrecipitationDistributionSubpolar);
        BalanceDistribution(PrecipitationDistributionBoreal);
        BalanceDistribution(PrecipitationDistributionCool);
        BalanceDistribution(PrecipitationDistributionWarm);
        BalanceDistribution(PrecipitationDistributionSubtropical);
    }

    private void BalanceDistribution(float[] distribution)
    {
        double sum = 0;
        for (int i = 0; i < distribution.Length; i++)
        {
            sum += distribution[i];
        }
        if (sum == 0.0f)
        {
            for (int i = 0; i < distribution.Length; i++)
            {
                distribution[i] = 1.0f / distribution.Length;
                sum += distribution[i];
            } 
        }

        for (int i = 0; i < distribution.Length; i++)
        {
            distribution[i] = (float)(distribution[i] / sum);
        }
        sum = 0;
        for (int i = 0; i < distribution.Length; i++)
        {
            sum += distribution[i];
        }
        Debug.AssertFormat(sum >= 1.0f, "BalanceDistribution, sum is not greater of equal to zero: {0}", sum);
        Debug.LogFormat("BalanceDistribution, sum is: {0} {1}", sum, (float)sum);
    }
    public float[] GetPrecipitationDistribution(BiomeRegion region)
    {

        switch (region)
        {
            case BiomeRegion.Polar:
                return PrecipitationDistributionPolar;
            case BiomeRegion.Subpolar:
                return PrecipitationDistributionSubpolar;
            case BiomeRegion.Boreal:
                return PrecipitationDistributionBoreal;
            case BiomeRegion.Cool:
                return PrecipitationDistributionCool;
            case BiomeRegion.Warm:
                return PrecipitationDistributionWarm;
            case BiomeRegion.Subtropical:
                return PrecipitationDistributionSubtropical;
            case BiomeRegion.Tropical:
                return PrecipitationDistributionTropical;
        }
        return null;
     }
}
public class LocalClimate
{
    public float[] PrecipitationDistribution { get; private set;}
    public float PrecipitationIntesityMean { get; private set;}
    public float PrecipitationIntesityDev { get; private set;}

    private VoronoiCell cell;

    public LocalClimate(VoronoiCell cell, Biome biome)
    {
        this.cell = cell;
        Climate.Instance.GetPrecipitationDistribution(biome.region);
    }

    public LocalClimate(float[] precipitationDistribution,
                   float precipitationIntesityMean,
                   float precipitationIntesityDev)
    {

        Debug.AssertFormat(precipitationDistribution.Length != Date.daysInMonth.Length,
            "Illegal precipitationDistribution length: {0}, expected: {1}", 
            precipitationDistribution.Length, 
            Date.daysInMonth.Length);

        PrecipitationDistribution = new float[Date.daysInMonth.Length];
    }
}