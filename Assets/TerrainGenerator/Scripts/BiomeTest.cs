using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeTest : MonoBehaviour
{
    float precipitation = 1500f;
    float biotemprature = 4.0f; 
    // Start is called before the first frame update
    void Start()
    {
        Biome biome = new Biome(precipitation, biotemprature);
        Debug.LogFormat("Biome");
        Debug.LogFormat("    precipitation: {0}", biome.precipitation);
        Debug.LogFormat("    biotemprature: {0}", biome.biotemprature);
        Debug.LogFormat("    evapotranspiration: {0}", biome.evapotranspiration);
        Debug.LogFormat("    region: {0}", biome.region);
        Debug.LogFormat("    type: {0}", biome.type);
        Debug.LogFormat("    subType: {0}", biome.subType);
        Debug.LogFormat("    humidity: {0}", biome.humidity);
        Debug.LogFormat("    altitude: {0}", biome.altitude);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
