using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public enum SweepDirection {FromCenter, FromBoarder};

public class Graph
{
    public static Graph TheGraph { get; private set; }  
    // What data do we really need, everything can be extracted from the edges.
    public VoronoiCell[] VoronoiCells { get; }  
    public VoronoiEdge[] VoronoiEdges { get; }  

    public Vector3[] DelaunayVertices { get;  }
    public VoronoiVertex[] VoronoiVertices { get; }

    public Rect Bounds { get;}
    public float MaxDistance { get; }
    public Vector3[] points; // Delaunay points;
    public int[] triangles; // polygon triangles
    public Vector3[] CellVertices; // Voronoi polygon corners;

    // TODO: who should own the meshObject?
    public GameObject meshObject;
    public List<Material> mapMaterials;
    //public Edge[] edges;

    public Graph(VoronoiCell[] cells, VoronoiEdge[] edges, Vector3[] delaunayVertices, VoronoiVertex[] voronoiVertices, Vector3[] cellVertices, Rect bounds)
    {
        VoronoiCells = cells;
        VoronoiEdges = edges; 
        DelaunayVertices  = delaunayVertices;
        VoronoiVertices = voronoiVertices;
        CellVertices = cellVertices;
        Bounds = bounds;
        MaxDistance = Mathf.Sqrt(bounds.width * bounds.width + bounds.height * bounds.height) / 2.0f;
        TheGraph = this;
    }
#if NDEF
    public VoronoiCell FindCell(Vector3 point)
    {
        foreach(var cell in VoronoiCells)
        { 
            if (cell.IsPointInCell2D(point))
            {
                return cell;
            }
        }
        return null;
    }
#endif
    float Vector2SquaredDistance(Vector2 a, Vector2 b)
    {
        return (a-b).sqrMagnitude;
    }
    public VoronoiCell FindCell(Vector2 point)
    {
        float minDist = float.MaxValue;
        int ix = -1;
        for(int i = 0; i < DelaunayVertices.Length; i++)
        {
            float dist = Vector2SquaredDistance(point, DelaunayVertices[i]);
            if (dist < minDist)
            {
                minDist = dist;
                ix = i;
            }
        }
        if (ix == -1)
        {
            Debug.LogError("No cell was found");
            return null;
        }
        return VoronoiCells[ix];
    }
    public VoronoiEdge FindEdge(Vector3 point)
    {
        VoronoiCell cell = FindCell(point);
        if (cell == null)
            return null;
        return cell.Borders[0];
    }
    public VoronoiVertex FindVertex(Vector3 point)
    {
        VoronoiCell cell = FindCell(point);
        if (cell == null)
            return null;
        return cell.Vertices[0];
    }
    public float RelativeDistanceFromCenterSquared2D(Vector3 position)
    {
        float x = position.x - Bounds.center.x;
        float y = position.y - Bounds.center.y;
        return (x * x + y * y) / (MaxDistance * MaxDistance);
    }
    public float RelativeDistanceFromCenter2D(Vector3 position)
    {
        return Vector3.Distance(position, Bounds.center) / MaxDistance;
    }
    public float PerlinNoiseInMap(Vector3 position, float frequency, float xOffset, float yOffset)
    {
        //Debug.LogFormat("PerlinNoiseInMap({0}, {1}, {2}, {3} bounds: {4}", position, frequency, xOffset, yOffset, Bounds);
        float xCoord = position.x / Bounds.width * frequency - Bounds.xMin + xOffset;
        float yCoord = position.y / Bounds.height * frequency - Bounds.yMin + yOffset; 
        float sample = Mathf.PerlinNoise(xCoord, yCoord);
        
        return sample;
    }
    public IEnumerable<VoronoiCell> CellSweep(SweepDirection direction = SweepDirection.FromCenter, List<VoronoiCell> start = null)
    {
        Debug.Log("CellSweep");

        Queue<VoronoiCell> queue = new Queue<VoronoiCell>();
        HashSet<VoronoiCell> visited = new HashSet<VoronoiCell>();

        if (start != null)
        {
            foreach (var cell in start)
            {
               queue.Enqueue(cell);
               visited.Add(cell); 
            }
        }
        else if (direction == SweepDirection.FromCenter)
        {
            VoronoiCell cell = FindCell(Bounds.center);

            Debug.Assert(cell != null, "No center cell is found!");
            if (cell == null) yield break;
            queue.Enqueue(cell);
            visited.Add(cell);
        }
        else if (direction == SweepDirection.FromBoarder)  
        {
            foreach (var cell in VoronoiCells)
            {
                if (cell.isBorder)
                { 
                    queue.Enqueue(cell);
                    visited.Add(cell);
                }
            }
        }
        else
        {
            Debug.LogError("RadialSweep: Unknown direction!");
            yield break;
        }

        while (queue.Count > 0)
        {
            VoronoiCell cell = queue.Dequeue();
            yield return cell;

            foreach (var neigh in cell.Neighbors)
            {
                if (!visited.Contains(neigh))
                {
                    queue.Enqueue(neigh);
                    visited.Add(neigh);
                }
            }
        }
 
        yield break;
    }
    // For the sake of simplicity the first border edge of the cell
    // in the center of the map is taken as the start edge in case
    // sweep direction is FromCenter.
    public IEnumerable<VoronoiEdge> EdgeSweep(SweepDirection direction =SweepDirection.FromCenter, List<VoronoiEdge> start = null)
    {
        Debug.Log("EdgeSweep");

        Queue<VoronoiEdge> queue = new Queue<VoronoiEdge>();
        HashSet<VoronoiEdge> visited = new HashSet<VoronoiEdge>();

        if (start != null)
        {
            foreach (var edge in start)
            {
                queue.Enqueue(edge);
                visited.Add(edge);
            }
        }
        else if (direction == SweepDirection.FromCenter)
        {
            VoronoiEdge edge = FindEdge(Bounds.center);

            Debug.Assert(edge != null, "No center cell is found!");
            if (edge == null) yield break;
            queue.Enqueue(edge);
            visited.Add(edge);
        }
        else if (direction == SweepDirection.FromBoarder)  
        {
            foreach (var cell in VoronoiCells)
            {
                if (cell.isBorder)
                {
                    foreach (var edge in cell.Borders)
                    {
                        queue.Enqueue(edge);
                        visited.Add(edge);
                        break;
                    }
                } 
            }
        }
        else
        {
            Debug.LogError("RadialEdgeSweep: Unknown direction!");
            yield break;
        }

        while (queue.Count > 0)
        {
            VoronoiEdge edge = queue.Dequeue();
            
            yield return edge;

            foreach (var e in edge.V0.protrudes)
            {
                if (!visited.Contains(e))
                {
                    queue.Enqueue(e);
                    visited.Add(e);
                }
            }

            foreach (var e in edge.V1.protrudes)
            {
                if (!visited.Contains(e))
                {
                    queue.Enqueue(e);
                    visited.Add(e);
                }
            }
        }  

        yield break;
    }
    // For the sake of simplicity the first vertex of the cell
    // in the center of the map is taken as the start vertex in case
    // sweep direction is FromCenter.
    public IEnumerable<VoronoiVertex> VertexSweep(SweepDirection direction =SweepDirection.FromCenter, List<VoronoiVertex> start = null)
    {
        Debug.Log("VertexSweep");

        Queue<VoronoiVertex> queue = new Queue<VoronoiVertex>();
        HashSet<VoronoiVertex> visited = new HashSet<VoronoiVertex>();

        if (start != null)
        {
            foreach (var vertex in start)
            {
                queue.Enqueue(vertex);
                visited.Add(vertex);
            }
        }
        else if (direction == SweepDirection.FromCenter)
        {
            VoronoiVertex vertex = FindVertex(Bounds.center);

            Debug.Assert(vertex != null, "No center cell is found!");
            if (vertex == null) yield break;
            queue.Enqueue(vertex);
            visited.Add(vertex);
        }
        else if (direction == SweepDirection.FromBoarder)  
        {
            foreach (var cell in VoronoiCells)
            {
                if (cell.isBorder)
                {
                    foreach (var edge in cell.Borders)
                    {
                        if (!visited.Contains(edge.V0))
                        {
                            queue.Enqueue(edge.V0);
                            visited.Add(edge.V0);
                        }
                        if (!visited.Contains(edge.V1))
                        {
                            queue.Enqueue(edge.V1);
                            visited.Add(edge.V1);
                        }
                        break;
                    }
                } 
            }
        }
        else
        {
            Debug.LogError("RadialVertexSweep: Unknown direction!");
            yield break;
        }
        while (queue.Count > 0)
        {
            VoronoiVertex vertex = queue.Dequeue();

            yield return vertex;

            foreach (var e in vertex.adjacent)
            {
                if (!visited.Contains(e))
                {
                    queue.Enqueue(e);
                    visited.Add(e);
                }
            }
        }

        yield break;
    }
}