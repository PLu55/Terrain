using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Unity.VisualScripting;
using UnityEngine;

public class RiverBuilder
{
    MapEditor mapEditor;
    Graph graph;

    public RiverBuilder(MapEditor mapEditor, Graph graph)
    {
        this.mapEditor = mapEditor;
        this.graph = graph;
    }
    public void Build()
    {
        
        mapEditor.CreateRiverContainer();
        if (mapEditor.createRivers)
            CreateRivers();
    }
    void CreateRivers()
    {
        Debug.Log("CreateRivers");
        float riverRate = 0.4f;
        float scale = 1.0f / mapEditor.ElevationScale;
        foreach (var cell in graph.VoronoiCells)
        {
            float p = UnityEngine.Random.value;
            if (!cell.isOcean && p <= riverRate * cell.elevation * scale)
            {
               CreateRiver(cell);
            } 
        }
    }

    void CreateRiver(VoronoiCell startCell)
    {
        Debug.LogFormat("CreateRiver from cell: {0} pos: {1}", startCell, startCell.GetCenter());
        float elevation = float.MinValue;
        VoronoiVertex top = null;
        List<Vector3> riverPath = new List<Vector3>();
        HashSet<Vector3> visited = new HashSet<Vector3>();

        // Select the vertex with highest elevation
        foreach(var v in startCell.Vertices)
        {
            if (v.elevation > elevation)
            {
               elevation = v.elevation;
               top = v;
            }
        }
        Debug.Assert(top != null, "Coulden't find the highest vertex");
        
        VoronoiEdge currentEdge = null;
        // Find which edge the vertex belongs to and add it to the riverPath
        foreach (var e in top.protrudes)
        {
            if (e.V0 == top)
            {
                currentEdge = e;
                riverPath.Add(currentEdge.V0.GetVertex());
                break;
            } 
            else if (e.V1 == top)
            {
                currentEdge = e;
                riverPath.Add(currentEdge.V1.GetVertex());
                break;  
            }
        }
        int loopCnt = 100;        
        // Traverse the edges with the speepest sloop downwards
        do
        {
            VoronoiVertex currentVertex;

            // The next vertex in the path is the one which has
            // the lowest elevation of the ends of the current edge.
            if (currentEdge.V0.elevation >= currentEdge.V1.elevation)
            {
                currentVertex = currentEdge.V1;
                riverPath.Add(currentEdge.V1.GetVertex());

                Debug.LogFormat("   currentVertex V1 {0}", currentVertex.GetVertex());
                Debug.LogFormat("   otherVertex V0 {0}", currentEdge.V0.GetVertex());              
            }
            else
            {
                currentVertex = currentEdge.V0;
                riverPath.Add(currentEdge.V0.GetVertex());
                Debug.LogFormat("   currentVertex V0 {0}", currentVertex.GetVertex());
                Debug.LogFormat("   otherVertex V1 {0}", currentEdge.V1.GetVertex());              
            }
            
            Vector3 pos = currentVertex.GetVertex();
            if (visited.Contains(pos))
            {
                Debug.LogWarning("A loop is detected while creating a river");
                break;
            }
            visited.Add(pos);

            VoronoiEdge nextEdge = null;
            float minSloop = float.MaxValue;

            // Select next edge
            foreach (var e in currentVertex.protrudes)
            {
                float sloop;

                if (currentVertex == e.V0)
                    sloop = -e.sloop;
                else
                    sloop = e.sloop;
                if (sloop < minSloop)
                {
                    nextEdge = e;
                    minSloop = sloop;
                }
            }
            currentEdge = nextEdge;
            //Debug.LogFormat("   vertex: {0}", currentVertex, )
            
        } while (currentEdge != null && !currentEdge.IsCoastLine() && !DoesEdgeTouchWater(currentEdge) && loopCnt-- > 0);
        Vector3 lastPos = riverPath.Last();
        Debug.LogFormat("    to cell {0} at pos: {1}", graph.FindCell(lastPos), lastPos);
        Debug.LogFormat("    river length, number of verices {0}", riverPath.Count);
        DrawRiver(riverPath.ToArray());
    }
    private bool DoesEdgeTouchWater(VoronoiEdge edge)
    {
        return edge.D0.isWater || edge.D1.isWater;
    }
    void DrawRiver(Vector3[] points)
    {
        string name = "River";
        Color color = Color.blue;
        float width = 50;
        Vector3[] outPoints = new Vector3[points.Length];
        Vector3 off = new Vector3(0, 0, -0.5f);
        for (int i = 0; i < points.Length; i++)
        {
            outPoints[i] = points[i] + off;    
        }

        mapEditor.CreateLine(mapEditor.RiverContainer, name, outPoints, color, width);
    }

}
