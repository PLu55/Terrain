using System.Collections.Generic;
using System;
using UnityEngine;
using Unity.VisualScripting;
using System.Linq;

public class MapBuilder
{
    public float maxDistance; // half the diagonal of the bounds rectangle
    public float maxDistanceFromCoast;

    Graph graph;
    MapEditor mapEditor;

    public MapBuilder(MapEditor mapEditor, Graph graph)
    {
        this.graph = graph;
        this.mapEditor = mapEditor;
    }

    public void Build()
    {
        Func<float, float> elevation = distance => distance;
        CreateCoastline();
        CalculatElevations(elevation);
        CreateBiomes(mapEditor.seeLevelTemprature, mapEditor.precipitation);
        AttachCellTypes();
        CreateLakes();
        CreateMesh(mapEditor.applyElevation);
        CreateRivers();
    }
    public void CreateBiomes(float seeLevelTemprature, float precipitation, float lapseRate = -6.0f * 0.001f)
    {
        int typeM = (int)BiomeRegion.count;
        int subTypeM = typeM + (int)BiomeType.count;
        int n = (int)BiomeRegion.count * (int)BiomeType.count * (int)BiomeSubType.count;
        int[] biomes = new int[n];

        foreach (var cell in graph.VoronoiCells)
        {
            if (cell.isWater) continue;
            float T = seeLevelTemprature + cell.elevation * lapseRate;
            Biome biome = new Biome(precipitation, T);
            cell.biome = biome;
            //Debug.LogFormat("Cell: {0} has Biome: {1}, {2}, {3}", cell, biome.region, biome.type, biome.subType);
            int bix = Biome.EncodeBiomeTypes(biome.region, biome.type, biome.subType);
            //int bix = (int)biome.region + (int)biome.type * typeM + (int)biome.subType * subTypeM;  
            biomes[bix]++;
        }

        for (int i = 0; i < n; i++)
        {
            if (biomes[i] > 0)
            {
                int[] rts = Biome.DecodeBiomeTypes(i);
                Debug.LogFormat("Biome statistics: {0}, {1}, {2} found: {3} times", (BiomeRegion)rts[0], (BiomeType)rts[1], (BiomeSubType)rts[2], biomes[i]);
            }
        }
    }
    public void CalculateEdgeSloopes()
    {
        foreach (var edge in graph.VoronoiEdges)
        {
            edge.sloop = edge.Sloop();

            Vector3 v0 = edge.V0.GetVertex();
            Vector3 v1 = edge.V1.GetVertex();
        }
    }
    public void CalculatElevations(Func<float, float> func)
    {
        Debug.Log("CalculateElevation");
        CalculateDistanceFromCoast();

        foreach (var cell in graph.VoronoiCells)
        {
            float elevationSum = 0.0f;
            float relativeDistance = 1.0f - graph.RelativeDistanceFromCenter2D(graph.DelaunayVertices[cell.Index]);
            float distanceFromCoast = cell.distanceFromCoast / maxDistanceFromCoast;
            // Debug.LogFormat("relativeDistance: {0} distance from coast: {1}", relativeDistance, distanceFromCoast);

            foreach (var vertex in cell.Vertices)
            {
                if (cell.isOcean)
                {
                    vertex.elevation = 0.0f;
                    continue;
                }
                float r = mapEditor.ElevationRandomFactor;
                float pn = graph.PerlinNoiseInMap(graph.CellVertices[vertex.Index], mapEditor.ElevationFrequency, mapEditor.ElevationXOffset, mapEditor.ElevationYOffset);
                //float elevation = func(distance);
                float elevation = ((r * pn  * distanceFromCoast) + ((1.0f - r) * relativeDistance)) * mapEditor.ElevationScale;
                vertex.elevation = elevation;
                graph.CellVertices[vertex.Index].z = -vertex.elevation;
                elevationSum += elevation;
            }
            cell.elevation = elevationSum / cell.Vertices.Count;
        }
        CalculateEdgeSloopes();
    }
    public void CalculateDistanceFromCoast()
    {
        Debug.Log("CalculateDistanceFromCoast");

        List<VoronoiCell> start = new List<VoronoiCell>();
        HashSet<VoronoiCell> visited = new HashSet<VoronoiCell>();

        foreach (var cell in graph.VoronoiCells)
        {
            cell.distanceFromCoast = -1;
            if (cell.isCoast)
            {
                start.Add(cell);
                visited.Add(cell);
            }
        }

        maxDistanceFromCoast = 0;

        foreach (var cell in graph.CellSweep(start: start))
        {
            float min = float.MaxValue;
            if (cell.isOcean) continue;
            if (cell.isCoast)
            {
                cell.distanceFromCoast = 0;
            }
            else
            {
                foreach (var neigh in cell.Neighbors)
                {
                    if (visited.Contains(neigh) &&  !neigh.isOcean)
                    {
                        if (neigh.distanceFromCoast != -1)
                        {
                            float d = Vector3.Distance(graph.DelaunayVertices[cell.Index], graph.DelaunayVertices[neigh.Index]);
                            min = MathF.Min(min, neigh.distanceFromCoast + d);
                        }  
                    }
                }
                
                cell.distanceFromCoast = min;
                maxDistanceFromCoast = MathF.Max(maxDistanceFromCoast, cell.distanceFromCoast); 
            }
            visited.Add(cell);
        }
    }
    bool IsOcean(VoronoiCell cell)
    {
        //int cnt = 0;

        //foreach (var neigh in cell.Neighbors)
        //    if (neigh.ocean) cnt++;
    
        //float p = (float) cnt / cell.Neighbors.Count;
        float p = mapEditor.CoastlineRandomFactor;
        float d = graph.RelativeDistanceFromCenter2D(graph.DelaunayVertices[cell.Index]);
        //float d = RelativeDistanceFromCenterSquared2D(Graph.DelaunayVertices[cell.Index]);
        float sample = graph.PerlinNoiseInMap(graph.DelaunayVertices[cell.Index],  mapEditor.CoastlineFrequency,  mapEditor.CoastlineOffsetX,  mapEditor.CoastlineOffsetY);
        return (p * sample) + ((1.0f - p) * (1.0 - d)) < mapEditor.CoastlineThreshold;
    }

    public void CreateCoastline()
    {
        Debug.Log("CreateCoastline");
        List<VoronoiCell> front = new List<VoronoiCell>();
        List<VoronoiCell> next = new List<VoronoiCell>();
        HashSet<VoronoiCell> visited = new HashSet<VoronoiCell>();

        foreach (var cell in graph.VoronoiCells)
        {
            if (cell.isBorder) 
            {
                cell.isOcean = true;
                cell.isWater = true;
                //visited.Add(cell);
                front.Add(cell);       
            }
        }

        bool hasOcean;
        int coastlineGeneration = 0;
        
        do
        {
            hasOcean = false;
            
            foreach (var cell in front)
            {
                if (true)
                {
                    bool isOcean = IsOcean(cell);
                    hasOcean = isOcean || hasOcean;
                    cell.isOcean = isOcean | cell.isOcean;
                    cell.isWater = cell.isOcean;
                    visited.Add(cell);
                }

                foreach (var neigh in cell.Neighbors)
                {
                    if (!visited.Contains(neigh))
                    {
                        next.Add(neigh);
                    }
                }
            }
            //front.Clear();
            //front = new List<VoronoiCell>(next);
            front = next;
            //front.AddRange(next);
            //next.Clear();
            next = new List<VoronoiCell>();
        } while (hasOcean && 
                coastlineGeneration++ < mapEditor.CoastlineMaxGeneration &&
                visited.Count < graph.VoronoiCells.Length);

        foreach (var cell in graph.VoronoiCells)
        {
            if (!cell.isOcean)
            {
                foreach (var neigh in cell.Neighbors)
                {
                    if (neigh.isOcean)
                    {
                        cell.isCoast = true;
                        break;  
                    }
                }
            }
        }
    }
    public void AttachCellTypes()
    {
        Debug.Log("AttachCellTypes");
        LoadMapMaterials();
        foreach (var cell in graph.VoronoiCells)
        {
            /*
            foreach ( var neig in cell.Neighbors)
            {
                if (neig.isBorder) 
                    cell.isOcean = true;
            }
            */
            cell.type = CellType.GetType("Unknown");
            if (cell.isCoast)
            {
                cell.type = CellType.GetType("Beach");
            }
            if (cell.isOcean)
            {
                cell.type = CellType.GetType("Ocean");
                //Debug.LogFormat("Cell {0} has type: {1}", cell, cell.type);
            }
            if (cell.biome != null && cell.type == CellType.GetType("Unknown"))
            {
                string typeStr = cell.biome.type.ToString();
                string subtypeStr = cell.biome.subType.ToString();
                
                if (cell.biome.region == BiomeRegion.Polar)
                {
                    cell.type = CellType.GetType("Snow");
                }
                else if (CellType.HasCellType(typeStr+subtypeStr))
                {
                    cell.type = CellType.GetType(typeStr+subtypeStr);
                }
                else
                {
                    cell.type = CellType.GetType(typeStr);
                }
                /*
                else if (cell.biome.region == BiomeRegion.tropical)
                {
                    cell.type = CellType.GetType(typeStr);
                    if (cell.biome.type == BiomeType.forest)
                        cell.type = CellType.GetType("Forest");
                    else if (cell.biome.type == BiomeType.desert)
                        cell.type = CellType.GetType("Desert");
                    else if (cell.biome.type == BiomeType.woodland)
                        cell.type = CellType.GetType("Desert");
                }
                else if (cell.biome.region == BiomeRegion.warm)
                {
                    if (cell.biome.type == BiomeType.forest)
                        cell.type = CellType.GetType("Forest");
                    else if (cell.biome.type == BiomeType.stepp)
                        cell.type = CellType.GetType("Stepp");
                    else if (cell.biome.type == BiomeType.desert)
                        cell.type = CellType.GetType("Desert");
                }
                else if (cell.biome.region == BiomeRegion.cool)
                {
                    if (cell.biome.type == BiomeType.forest)
                        cell.type = CellType.GetType("Forest");
                    else if (cell.biome.type == BiomeType.stepp)
                        cell.type = CellType.GetType("Stepp");
                    else if (cell.biome.type == BiomeType.desert)
                        cell.type = CellType.GetType("Desert");

                }
                 else if (cell.biome.region == BiomeRegion.boreal)
                {
                    if (cell.biome.type == BiomeType.forest)
                        cell.type = CellType.GetType("Forest");
                    else if (cell.biome.type == BiomeType.desert)
                        cell.type = CellType.GetType("Desert");
                    else if (cell.biome.type == BiomeType.scrub)
                        cell.type = CellType.GetType("Scrub");

                }
                else if (cell.biome.region == BiomeRegion.subpolar)
                {
                    if (cell.biome.type == BiomeType.tundra)
                        cell.type = CellType.GetType("Tundra");
                }
                */
            }
            if (cell.isBorder)
            {
                //Debug.LogFormat("Cell {0} is border", cell);
            }
        } 
    }
    void LoadMapMaterials()
    {
        int index = -1;
        foreach (var mat in mapEditor.mapMaterials)
        {
            string name = mat.name.Replace("Material", "");
            CellType.CreateCellType(name, index++);
        } 
    }
    void ApplyElevationToMesh()
    {
        Debug.Log("ApplyElevationToMesh");
        Debug.Assert(graph.meshObject);
        var meshFilter = graph.meshObject.GetComponent<MeshFilter>();
        Debug.Assert(meshFilter);
        meshFilter.mesh.vertices = graph.CellVertices;
        meshFilter.mesh.MarkModified();
        meshFilter.mesh.RecalculateNormals();
        meshFilter.mesh.RecalculateBounds();
    }
    public void RefreshMesh()
    {
        Mesh mesh = graph.meshObject.GetComponent<MeshFilter>().mesh;
        mesh.RecalculateBounds();
        mesh.UploadMeshData(true);
    }
    public void CreateMesh(bool applyElevation)
    {
        Debug.Log("CreateMesh");
        //Debug.Log("Copying map materials");
        
        CellType ct_mesh = CellType.GetType("Mesh");
        CellType ocean = CellType.GetType("Ocean");

        //CellType[] cellTypes = new CellType[CellType.CellTypes.Count];

        
        if (graph.meshObject != null)
        {
           UnityEngine.Object.Destroy(graph.meshObject); 
        }

        Mesh mesh = new Mesh();
        mesh.vertices = graph.CellVertices;
        mesh.triangles = graph.triangles;

        List<List<int>> submeshes = new List<List<int>>();
        
        for (int i = 0; i < CellType.CellTypes.Count - 1; i++)
        {
            submeshes.Add(new List<int>());    
        }
        

        foreach (var cell in graph.VoronoiCells)
        {
            if (!(cell.type == null || cell.type == ct_mesh))
            {
                int ctIdx = cell.type.MaterialIndex;
                int i = cell.firstTriangle;
                int n = i + cell.numOfTriangles;
                // Debug.LogFormat("Attach material cell: {0}, type: {1} index: {2} tri: {3}-{4}", cell, cell.type, ctIdx, i, n-1);
                for (int j = i; j < n; j++)
                {
                   submeshes[ctIdx].Add(graph.triangles[j]); 
                }
            }
        }
        /*
        foreach (var cell in voronoiCells)
        {
            if (cell.type == ocean)
            {
                int i = cell.firstTriangle;
                for (int j = i; j < i + cell.numOfTriangles; j++)
                {
                   submeshes[0].Add(Graph.triangles[j]); 
                }
            }
        }
        */
        //mesh.SetTriangles(submeshes[0], 0);
        mesh.subMeshCount = CellType.CellTypes.Count;
        for (int i = 0; i < CellType.CellTypes.Count - 1; i++)
        {
            mesh.SetIndices(submeshes[i], MeshTopology.Triangles, i + 1);
        }
        
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        graph.meshObject = new GameObject("GraphMesh");
        var meshRenderer = graph.meshObject.AddComponent<MeshRenderer>();
        meshRenderer.sharedMaterials = mapEditor.mapMaterials.ToArray();
        var meshFilter = graph.meshObject.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        CreateMapCollider();
        if (applyElevation)
            ApplyElevationToMesh();
    }
    void CreateMapCollider()
    {
        BoxCollider boxCollider = graph.meshObject.AddComponent<BoxCollider>();

        // Adjust the size and center of the BoxCollider to fit the mesh
        // You may need to adjust these values based on the size and shape of your mesh
        ;
        boxCollider.size = new Vector3(graph.Bounds.width, graph.Bounds.height, 1);
        boxCollider.center = graph.Bounds.center;
    }
    void CreateRivers()
    {
        RiverBuilder riverBuilder= new RiverBuilder(mapEditor, graph);
        riverBuilder.Build();
    }
#if NDEF
    public List<List<VoronoiVertex>> FindDepressions(int searchLength)
    {
        List<VoronoiVertex> vertices = new List<VoronoiVertex>();

        List<List<VoronoiVertex>> depressions = new List<List<VoronoiVertex>>();

        foreach (var vertex in graph.VoronoiVertices)
        {
            float e0 = vertex.elevation;

            bool all = true;

            foreach (var v in vertex.adjacent)
            {
                if (v.elevation < e0)
                {
                    all = false;
                    break;
                }
            }
            if (all) 
            {
                depressions.Add(ExpandDepression(vertex));
            }
        }
        return depressions;
    }
    public List<VoronoiVertex> ExpandDepression(VoronoiVertex seed)
    {
        HashSet<VoronoiVertex> visited = new HashSet<VoronoiVertex>();
        Heap<VoronoiVertex> heap = new Heap<VoronoiVertex>(new VoronoiVertexElevationComparer());

        float planeZ = seed.elevation;
        heap.Enqueue(seed);
        visited.Add(seed);

        while (!heap.IsEmpty())
        {
            VoronoiVertex vertex = heap.Dequeue();
            planeZ = vertex.elevation;
            bool end = false;

            foreach (var vx in vertex.adjacent)
            {
                if (vx.elevation < planeZ)
                {
                    end = true;
                    break;
                }
                else if (!visited.Contains(vx))
                {
                    visited.Add(vx);
                }
            }
            if (end) break;
        }

        return visited.ToList();
    }
#endif
    public List<List<VoronoiCell>> FindDepressions()
    {
        List<VoronoiCell> vertices = new List<VoronoiCell>();

        List<HashSet<VoronoiCell>> depressions = new List<HashSet<VoronoiCell>>();
        HashSet<VoronoiCell> visited = new HashSet<VoronoiCell>();

        foreach (var cell in graph.VoronoiCells)
        {
            if (cell.isOcean || visited.Contains(cell)) continue;

            float e0 = cell.elevation;

            bool all = true;

            foreach (var v in cell.Neighbors)
            {
                if (v.elevation <= e0)
                {
                    all = false;
                    break;
                }
            }
            if (all)
            {
                HashSet<VoronoiCell> expand = ExpandDepression(cell);
                //if (expand.IntersectWith(visited))
                if (expand.Overlaps(visited))
                {
                    Debug.LogFormat("Depressions overlaps");
                    foreach (var dep in depressions)
                    {
                        if (expand.Overlaps(dep))
                        MergeDespressions(expand, dep);
                    }
                }
                visited.UnionWith (expand);
                depressions.Add(expand);
            }
        }
        List<List<VoronoiCell>> result = new List<List<VoronoiCell>>();
        foreach (var dep in depressions)
        {
            result.Add(dep.ToList());
        }
        // RefreshMesh();

        return result;
    }
    private void SetDepressionElevation(HashSet<VoronoiCell> depression, float elevation)
    {
        Debug.Log("SetDepressionElevation");
        foreach (var cell in depression)
        {
            cell.elevation = elevation;
            foreach (var vertex in cell.Vertices)
            {
                vertex.elevation = elevation;
                Vector3 v = vertex.GetVertex();
                v.z = -elevation;
            }
        }
    }
    private void MergeDespressions(HashSet<VoronoiCell> expandedDepression, HashSet<VoronoiCell> exploredDepression)
    {
        Debug.Log("Merge depressiion");
        HashSet<VoronoiCell>.Enumerator enum0 = expandedDepression.GetEnumerator();
        HashSet<VoronoiCell>.Enumerator enum1 = exploredDepression.GetEnumerator();
        enum0.MoveNext();
        enum1.MoveNext();

        VoronoiCell cell0 = enum0.Current;
        VoronoiCell cell1 = enum1.Current;
        Debug.Assert(cell0 != null, "MergeDespressions: cell0 is null");
        Debug.Assert(cell1 != null, "MergeDespressions: cell1 is null");

        if (cell0.elevation < cell1.elevation)
        {
            SetDepressionElevation(exploredDepression, cell0.elevation);
        }
        else if (cell0.elevation > cell1.elevation) 
        {
            SetDepressionElevation(expandedDepression, cell0.elevation);
        }
        exploredDepression.UnionWith(expandedDepression);
    }
    private HashSet<VoronoiCell> ExpandDepression(VoronoiCell seed)
    {
        HashSet<VoronoiCell> visited = new HashSet<VoronoiCell>();
        Heap<VoronoiCell> heap = new Heap<VoronoiCell>(new VoronoiCellElevationComparer());

        float planeZ;
        heap.Enqueue(seed);
        visited.Add(seed);

        while (!heap.IsEmpty())
        {
            VoronoiCell cell = heap.Dequeue();
            planeZ = cell.elevation;
            bool end = false;

            foreach (var vx in cell.Neighbors)
            {
                if (vx.elevation < planeZ)
                {
                    end = true;
                    break;
                }
                else if (!visited.Contains(vx))
                {
                    visited.Add(vx);
                }
            }
            if (end) break;
        }

        return visited;
    }
    public void CreateLakes()
    {
        List<List<VoronoiCell>> depressions = FindDepressions();

        foreach (var depression in depressions)
        {
            float minElevation = float.MaxValue;

            foreach (var cell in depression)
            {
                if (cell.isOcean) continue;
                if (cell.elevation <  minElevation)
                    minElevation = cell.elevation;
            }
            foreach (var cell in depression)
            {
                if (cell.isOcean) continue;
                cell.isWater = true;
                cell.type = CellType.GetType("Lake");
                cell.elevation = minElevation;

                foreach (var vertex in cell.Vertices)
                {
                    Vector3 v = vertex.GetVertex();
                    v.z = -minElevation;
                    vertex.elevation = minElevation;
                }
            }
        }

    }

}
