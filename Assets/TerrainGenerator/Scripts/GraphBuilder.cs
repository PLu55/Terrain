using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEditor;

using DelaunatorSharp;
using DelaunatorSharp.Unity;
using DelaunatorSharp.Unity.Extensions;
using System.Linq;

public class GraphBuilder
{
    MapEditor mapEditor;
    Graph Graph { get; set; }
    Delaunator delaunator;
    Vector3[] delaunayVertices;
    Dictionary<Vector3, VoronoiVertex> voronoiVertices;
    int voronoiVertexIndex;

    VoronoiCell[] voronoiCells;
    List<VoronoiEdge> voronoiEdges;

    public List<CellType> CellTypes { get; } = new List<CellType>();
    List<Material> mapMaterials;
    public Rect bounds;
    public float maxDistance; // half the diagonal of the bounds rectangle
    //public float maxDistanceFromCoast;

    public GraphBuilder(MapEditor mapEditor)
    {
        this.mapEditor = mapEditor;
        voronoiVertices = new Dictionary<Vector3, VoronoiVertex>();
        voronoiVertexIndex = 0;
        voronoiEdges = new List<VoronoiEdge>();
        this.mapMaterials = mapEditor.mapMaterials; 
    }
    public void Build()
    {
        CreateDelaunator();
        CreateCells();
        CreateEdges();
        CreateGraph();
        TriangulateCells();
        //CreateCoastline();
        //AttachCellTypes();
    }
    public Graph GetGraph()
    {
        return Graph;
    }

    void TriangulateCells()
    {
        List<int> vertices = new List<int>();

        VoronoiCellBuilder cellBuilder  = new VoronoiCellBuilder(Graph, Graph.CellVertices);

        foreach (var cell in voronoiCells)
        {
            cellBuilder.TriangulateCell(cell);          
        }
        Graph.triangles = cellBuilder.Triangles.ToArray();
        cellBuilder.CheckTriangulation();
    }

    public void CreateDelaunator()
    {
        float mapWidth = mapEditor.mapWidth;
        float mapHeight = mapEditor.mapHeight;
        float pointMinDistance = mapEditor.pointMinDistance;
        List<IPoint> points = new List<IPoint>();

        Vector2 topLeft = new Vector2(-mapWidth/2, -mapHeight/2);
        Vector2 bottomRight = new Vector2(mapWidth/2, mapHeight/2);

        var samples = UniformPoissonDiskSampler.SampleRectangle(topLeft, bottomRight, pointMinDistance);
        points = samples.Select(point => new Vector2(point.x, point.y)).ToPoints().ToList();
        Debug.Log($"Number of generated random points {points.Count}");
        if (points.Count < 3) 
        {
            Debug.LogError("Less then 3 points, can't create a Delaunator");
            return;
        }

        delaunator = new Delaunator(points.ToArray());
        delaunayVertices = delaunator.Points.ToVectors3();
        GetBoundsFromDelaunator();
    }
    void CreateCells()
    {
        HashSet<int> hull = new HashSet<int>(delaunator.Hull);
        int n = delaunayVertices.Length;
        voronoiCells = new VoronoiCell[n];
        int vertexIndex = 0;

        for (int pointIndex = 0; pointIndex < delaunayVertices.Length; pointIndex++)
        {
            VoronoiCell cell = new VoronoiCell(vertexIndex);
            cell.isBorder = hull.Contains(pointIndex);
            voronoiCells[vertexIndex++] = cell;
        }
    }    
    void CreateEdges()
    {
        Debug.Log("CreateEdges");

        int n = delaunayVertices.Length;
        bool[] visited = new bool[n];
        int m = delaunator.Halfedges.Length;
        
        bool[] cellCoverage = new bool[voronoiCells.Length];
        bool[] visitedHalfEdges = new bool[m];

        for (int edge = 0; edge < m; edge++)
        {
            int point = delaunator.Triangles[edge];
            int edgeOpposit = delaunator.Halfedges[edge];
            //Debug.AssertFormat(eop != -1, "Halfedge opposit doesn't exists,  eop == -1 e: {0}", e);
            if (edgeOpposit == -1) continue;
            int pointOpposit = delaunator.Triangles[edgeOpposit];
            VoronoiCell cell0 = voronoiCells[point];
            VoronoiCell cell1 = voronoiCells[pointOpposit];
            /*
            Debug.LogFormat("Visiting halfedge e: {0} oposit halfedge: {1} cell0: {2} cell1: {3}",
                edge, edgeOpposit, 
                cell0 == null ? "none" : cell0.Index,  
                cell1 == null ? "none" : cell1.Index);
                */

            if (visitedHalfEdges[edgeOpposit])
            {
                // Debug.Log("Halfedge already seen!");
                continue; 
            }
            visitedHalfEdges[edge] = true;

            //Debug.LogFormat("   Visiting cell: {0} p: {1} e: {2}", cell0.Index, point, edge);

            VoronoiVertex vertex0 = CreateOrGetVertex(Delaunator.TriangleOfEdge(edge));
            VoronoiVertex vertex1 = CreateOrGetVertex(Delaunator.TriangleOfEdge(edgeOpposit));

            VoronoiEdge voronoiEdge = new VoronoiEdge(cell0, cell1, vertex0, vertex1);
            if (voronoiEdge != null)
            {
                // Debug.LogFormat("   creating edge");
                vertex0.protrudes.Add(voronoiEdge);
                vertex0.adjacent.Add(vertex1);
                if (!vertex0.touches.Contains(cell0)) vertex0.touches.Add(cell0);
                if (!vertex0.touches.Contains(cell1)) vertex0.touches.Add(cell1);
                vertex1.protrudes.Add(voronoiEdge);
                vertex1.adjacent.Add(vertex0);
                if (!vertex1.touches.Contains(cell0)) vertex1.touches.Add(cell0);
                if (!vertex1.touches.Contains(cell1)) vertex1.touches.Add(cell1);
            }
            else
            {
               Debug.LogErrorFormat("CreateEdges:  edge is null, could not be created"); 
            }

            Debug.AssertFormat(cell0 != null,"Cell is null, pointIndex {0}", point);
            Debug.AssertFormat(cell1 != null,"Cell is null, pointIndex {0}", pointOpposit);

            if (cell0 != null)
             {
                if (cell1 != null) cell0.Neighbors.Add(cell1);
                cell0.Borders.Add(voronoiEdge);
                Debug.AssertFormat(!cell0.Vertices.Contains(voronoiEdge.V0),
                    "cell0: {0} already contains vertex {1}",
                    cell0.ToString(), voronoiEdge.V0.ToString());
                cell0.Vertices.Add(voronoiEdge.V0);
                //Debug.LogFormat("   adding vertex: {0} to cell: {1}", voronoiEdge.V0.Index, cell0.Index);
            }
            if (cell1 != null)
            {
                if (cell0 != null) cell1.Neighbors.Add(cell0);
                cell1.Borders.Add(voronoiEdge);
                Debug.AssertFormat(!cell1.Vertices.Contains(voronoiEdge.V1),
                    "cell0: {0} already contains vertex {1}",
                    cell1.ToString(), voronoiEdge.V1.ToString());
                cell1.Vertices.Add(voronoiEdge.V1);
                //Debug.LogFormat("   adding vertex: {0} to cell: {1}", voronoiEdge.V0.Index, cell1.Index);
            }
            voronoiEdges.Add(voronoiEdge);
        }
        Debug.LogFormat("   number of voronoiEdges created: {0}", voronoiEdges.Count);
    }

    void FixBoarderCells()
    {
        foreach (var cell in voronoiCells)
        {
            if (cell.isBorder)
            {
                //???
            }   
        }
    }
    VoronoiVertex CreateOrGetVertex(int t)
    {
        if (t == -1) return null;

        // Debug.LogFormat("Create vertex: {0}", voronoiVertexIndex);
        VoronoiVertex vertex;

        Vector3 pnt = Delaunator.GetCentroid(delaunator.GetTrianglePoints(t)).ToVector3();
        if (voronoiVertices.ContainsKey(pnt))
        {
            vertex = voronoiVertices[pnt];
            //Debug.LogFormat("   found existing vertex at: {0}", pnt);
        }
        else
        {
            vertex = new VoronoiVertex(voronoiVertexIndex++);
            voronoiVertices.Add(pnt, vertex);
            //Debug.LogFormat("   creating new vertex at: {0}", pnt);
        }
        return vertex;
    }
    void CreateGraph()
    {
        Debug.Log("CreateGraph");

        // How to sort corners, they should be sorted clockwise around the center and grouped
        // by cell.
        Vector3[] corners = new Vector3[voronoiVertices.Count];

        foreach (var pair in voronoiVertices)
        {
            corners[pair.Value.Index] = pair.Key;
        } 

        Graph = new Graph(voronoiCells, voronoiEdges.ToArray(), delaunayVertices, voronoiVertices.Values.ToArray(), corners, bounds);
    }
    private void GetBoundsFromDelaunator()
    {
        float xmin = float.MaxValue;
        float xmax = float.MinValue;
        float ymin = float.MaxValue;
        float ymax = float.MinValue;

        foreach (var index in delaunator.Hull)
        {
            Vector3 vertex = delaunayVertices[index];
            xmin = Mathf.Min(vertex.x, xmin);
            xmax = Mathf.Max(vertex.x, xmax);
            ymin = Mathf.Min(vertex.y, ymin);
            ymax = Mathf.Max(vertex.y, ymax);
        }
        Rect rect = new Rect(xmin, ymin, xmax - xmin, ymax - ymin);
        Debug.LogFormat("Bounds are: {0} ", rect);
        bounds = rect;
        maxDistance = Mathf.Sqrt(bounds.width * bounds.width + bounds.height * bounds.height) / 2.0f;
    }

    public void ClearCells()
    {
        foreach (var cell in voronoiCells)
        {
            cell.Clear();
        }
    }
    public List<VoronoiCell> FindBasins(int searchLength)
    {
        List<VoronoiCell> cells = new List<VoronoiCell>();
        
        foreach (var cell in voronoiCells)
        {
            bool all = true;
            foreach (var neig in cell.Neighbors)
            {
                if(neig.elevation <= cell.elevation)
                {
                    all = false;
                    break;
                }
            }

            if (all)
            {
                Debug.LogFormat("Cell: {0} is a basin", cell);
                cell.type = CellType.CellTypes["Lake"];
                cell.isWater = true;
                cells.Add(cell);
            }
        }

        return cells;
    }
    public void CreateLakes()
    {
        List<VoronoiCell> cells = FindBasins(5);

        

    }
}