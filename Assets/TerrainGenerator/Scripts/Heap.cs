using System;
using System.Collections.Generic;

public class Heap<T>
{
    private List<T> heap;
    private IComparer<T> comparer;

    public Heap(IComparer<T> comparer)
    {
        this.comparer = comparer;
        heap = new List<T>();
    }

    public void Enqueue(T value)
    {
        heap.Add(value);
        HeapifyUp(heap.Count - 1);
    }

    public T Dequeue()
    {
        if (heap.Count == 0)
            throw new InvalidOperationException("Heap is empty");

        T minValue = heap[0];
        heap[0] = heap[heap.Count - 1];
        heap.RemoveAt(heap.Count - 1);
        HeapifyDown(0);
        return minValue;
    }
    public int Count()
    {
        return heap.Count;
    }
    public bool IsEmpty()
    {
        return heap.Count == 0;
    }
    public void Clear()
    {
        heap.Clear();
    }

    private void HeapifyUp(int index)
    {
        while (index > 0)
        {
            int parentIndex = (index - 1) / 2;
            if (comparer.Compare(heap[index],(heap[parentIndex])) < 0)
            {
                Swap(index, parentIndex);
                index = parentIndex;
            }
            else
            {
                break;
            }
        }
    }

    private void HeapifyDown(int index)
    {
        while (true)
        {
            int leftChild = 2 * index + 1;
            int rightChild = 2 * index + 2;
            int smallest = index;

            if (leftChild < heap.Count && comparer.Compare(heap[leftChild], heap[smallest]) < 0)
                smallest = leftChild;

            if (rightChild < heap.Count && comparer.Compare(heap[rightChild], heap[smallest]) < 0)
                smallest = rightChild;

            if (smallest != index)
            {
                Swap(index, smallest);
                index = smallest;
            }
            else
            {
                break;
            }
        }
    }

    private void Swap(int i, int j)
    {
        T temp = heap[i];
        heap[i] = heap[j];
        heap[j] = temp;
    }
}