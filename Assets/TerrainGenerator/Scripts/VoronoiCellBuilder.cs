using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class VoronoiCellBuilder
{
    Graph graph;
    Vector3[] vertices;
    public List<int> Triangles { get; } 

    public VoronoiCellBuilder(Graph graph, Vector3[] vertices)
    {
        this.graph = graph;
        this.vertices = vertices;
        Triangles = new List<int>();
    }
    // TODO: optimize, the triangles List can be build without appending sub results
    public void TriangulateCell(VoronoiCell cell)
    {
        List<int> vertexIndices = new List<int>();
        List<int> result = null;
        int n = cell.Vertices.Count;

        // TODO: This should not be needed, fix the problem! Done!
        //List<VoronoiVertex> Vertices = RemoveDuplicateVertices(cell.Vertices);
        List<VoronoiVertex> Vertices = cell.Vertices;

        //Debug.LogFormat("Triangulate cell: {0} which has {1} vertices, should be {2}", cell.ToString(), Vertices.Count, n);
        foreach (var vertex in Vertices)
        {
            //Debug.LogFormat("   vertex: {0} at {1}", vertex.Index, vertices[vertex.Index]);
            vertexIndices.Add(vertex.Index);
        }
        HashSet<int> indices = new HashSet<int>();
        indices.AddRange(vertexIndices);
        // TODO: optimize, reuse the list
        vertexIndices = SortVerticesCounterClockwise(vertexIndices);
        // Debug.LogFormat("   Sorted indices count: {0}", vertexIndices.Count);

        bool isConvex = IsPolygonConvex(vertexIndices);
        // Debug.LogFormat("   isConvex: {0}", isConvex);
        if (isConvex)
        {
            result =  TriangulatePolygonByFan(vertexIndices);
        }
        else
        {
            // Try to use the Delaunator to triangulate
            //result = TriangulatePolygonByEarClipping(vertexIndices);
        }
        // TODO: FIX: no result here when cell isn't convex!!!
        if (result != null)
        {
            // Debug.LogFormat("   adding: {0} triangles", result.Count/3);
            Debug.AssertFormat(result.Count/3 == Vertices.Count -2,
                "Number of triangles are not the expected {0}", result.Count/3);
            cell.firstTriangle = Triangles.Count;
            cell.numOfTriangles = result.Count;
            int m = Triangles.Count;
            Triangles.AddRange(result);
            Debug.AssertFormat(Triangles.Count == m + result.Count,
                "Number of added triangle vertices are not what is expected");
        
            foreach (var r in result)
            {
                Debug.AssertFormat(indices.Contains(r), 
                "Triangle index {0} is not a vertex of the cell {1}", cell, r);
            }
        }
    }
    List<int> TriangulatePolygonByFan(List<int> vertexIndices)
    {
        List<int> result = new List<int>();

        int center = vertexIndices[0];
        for (int i = 1; i < vertexIndices.Count - 1; i++)
        {
            result.Add(center);
            result.Add(vertexIndices[i]);
            result.Add(vertexIndices[i+1]);
        }
        return result;
    }
    List<int> TriangulatePolygonByEarClipping(List<int> polygonIndices)
    {
        List<int> indices = new List<int>();

        // Ensure polygon has enough vertices to triangulate
        if (polygonIndices.Count < 3)
        {
            Debug.LogWarning("Cannot triangulate polygon with less than 3 vertices.");
            return indices;
        }

        List<int> indicesList = new List<int>(polygonIndices);

        // Loop until polygon is reduced to a triangle
        while (indicesList.Count > 3)
        {
            // Find an ear (a vertex that forms a triangle with its adjacent vertices)
            int earIndex = FindEar(indicesList);

            // Add triangle indices for the ear
            indices.Add(indicesList[earIndex - 1]);
            indices.Add(indicesList[earIndex]);
            indices.Add(indicesList[earIndex + 1]);

            // Remove the ear vertex from the polygon
            indicesList.RemoveAt(earIndex);
        }

        // Add indices for the remaining triangle
        indices.Add(indicesList[0]);
        indices.Add(indicesList[1]);
        indices.Add(indicesList[2]);

        return indices;
    }
    int FindEar(List<int> indices)
    {
        int vertexCount = indices.Count;

        for (int i = 0; i < vertexCount; i++)
        {
            int previousIndex = (i - 1 + vertexCount) % vertexCount;
            int nextIndex = (i + 1) % vertexCount;

            int prevIndex = indices[previousIndex];
            int currentIndex = indices[i];
            int nextVertexIndex = indices[nextIndex];

            // Check if vertex forms an ear (no other vertices are inside the triangle)
            bool isEar = IsTriangleConvex(prevIndex, currentIndex, nextVertexIndex) && !IsPointInsideTriangle(prevIndex, currentIndex, nextVertexIndex, indices);
            if (isEar)
            {
                return i;
            }
        }

        // No ear found
        return -1;
    }
    bool IsTriangleConvex(int prevIndex, int currentIndex, int nextIndex)
    {
        Vector3 prevVertex = vertices[prevIndex];
        Vector3 currentVertex = vertices[currentIndex];
        Vector3 nextVertex = vertices[nextIndex];

        Vector2 prev = new Vector2(prevVertex.x, prevVertex.y);
        Vector2 current = new Vector2(currentVertex.x, currentVertex.y);
        Vector2 next = new Vector2(nextVertex.x, nextVertex.y);

        Vector2 side1 = current - prev;
        Vector2 side2 = next - current;

        float crossZ = side1.x * side2.y - side1.y * side2.x;

        return crossZ > 0f; // Convex if cross product is positive
    }

    bool IsPointInsideTriangle(int v1, int v2, int v3, List<int> indices)
    {
        Vector3 v1Vertex = vertices[v1];
        Vector3 v2Vertex = vertices[v2];
        Vector3 v3Vertex = vertices[v3];

        Plane trianglePlane = new Plane(v1Vertex, v2Vertex, v3Vertex);

        foreach (int index in indices)
        {
            Vector3 vertex = vertices[index];
            if (trianglePlane.GetSide(vertex))
            {
                return false; // Point is outside the triangle
            }
        }

        return true; // All points are on the same side of the triangle plane
    }
    List<int> SortVerticesCounterClockwise(List<int> vertexIndices)
    {
        List<int> sortedVertices = new List<int>(vertexIndices);
        Vector3 centroid = CalculateCentroid(vertexIndices);

        // Sort vertices based on their angles
        sortedVertices.Sort((vix0, vix1) =>
        {
            Vector3 directionA = vertices[vix0] - centroid;
            Vector3 directionB = vertices[vix1] - centroid;
            float angleA = Mathf.Atan2(directionA.y, directionA.x);
            float angleB = Mathf.Atan2(directionB.y, directionB.x);
            return angleB.CompareTo(angleA);
        });

        return sortedVertices;
    }
    Vector3 CalculateCentroid(List<int> vertexIndices)
    {
        Vector3 centroid = Vector3.zero;
        foreach (int index in vertexIndices)
        {
            centroid += vertices[index];
        }
        centroid /= vertexIndices.Count;
        return centroid;
    }
    bool IsPolygonConvex(List<int> indices)
    {
        if (indices.Count < 3)
        {
            Debug.LogWarning("Polygon has less than 3 vertices. Unable to determine convexity.");
            return false;
        }
        bool hasPositiveCrossProduct = false;
        bool hasNegativeCrossProduct = false;

        for (int i = 0; i < indices.Count; i++)
        {
            Vector3 currentVertex = vertices[indices[i]];
            Vector3 nextVertex = vertices[indices[(i + 1) % indices.Count]];
            Vector3 prevVertex = vertices[indices[(i + indices.Count - 1) % indices.Count]];

            Vector3 edge1 = nextVertex - currentVertex;
            Vector3 edge2 = prevVertex - currentVertex;

            // Calculate the cross product of consecutive edges
            Vector3 crossProduct = Vector3.Cross(edge1, edge2);

            // Determine the sign of the cross product
            float sign = Mathf.Sign(Vector3.Dot(crossProduct, Vector3.forward)); // Assuming Z-axis is up

            if (sign > 0)
            {
                hasPositiveCrossProduct = true;
            }
            else if (sign < 0)
            {
                hasNegativeCrossProduct = true;
            }

            // If both positive and negative cross products are found, the polygon is concave
            if (hasPositiveCrossProduct && hasNegativeCrossProduct)
            {
                return false;
            }
        }

        // If all cross products have the same sign, the polygon is convex
        return true;
    }
    List<VoronoiVertex> RemoveDuplicateVertices(List<VoronoiVertex> vertices)
    {
        HashSet<VoronoiVertex> seenV = new HashSet<VoronoiVertex>();
        HashSet<int> seenI = new HashSet<int>();
        List<VoronoiVertex> result = new List<VoronoiVertex>();

        foreach (var vertex in vertices)
        {
            if (seenV.Contains(vertex)) continue;
            if (seenI.Contains(vertex.Index)) continue;
            seenV.Add(vertex);
            seenI.Add(vertex.Index);
            result.Add(vertex);
        }
        return result;
    }

    public void CheckTriangulation()
    {
        foreach (var cell in graph.VoronoiCells)
        {
            int n = cell.firstTriangle + cell.numOfTriangles;
            Debug.AssertFormat(n <= graph.triangles.Length,"In cell: {0} the triangle index exceeds the size of Graph.triangles array", cell);

            int[] vertices = cell.GetVertexIndices();

            for (int i = cell.firstTriangle; i < n; i++)
            {
                bool found = false;
                for (int j = 0; j < vertices.Length; j++ )
                {
                    if (graph.triangles[i] ==  vertices[j])
                    {
                        found = true;
                        break;
                    }
                }
                Debug.AssertFormat(found, "In cell {0} an triangle index that does not belong to the cell is found", cell);
            }
        }
    }

}
