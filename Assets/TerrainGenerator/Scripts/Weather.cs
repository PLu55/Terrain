using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.AI;

public class Weather : MonoBehaviour
{
    private Graph graph;
    
    public float zone = 0;
    Double nextUpdate;
    Double timeStamp;
    public float updateInterval = 3600.0f; // in seconds
    public LocalWeather[] localWeather; 
    void Start()
    {
        graph = Graph.TheGraph;
        nextUpdate = 0.0;
        timeStamp = 0.0f;
        CollectCells();
    }

    // Update is called once per frame
    void Update()
    {
        if (nextUpdate <= World.CurrentTime)
            ComputeWeather();
            timeStamp = Time.timeAsDouble;
    }
    private void CollectCells()
    {
        List<LocalWeather> localWeatherList = new List<LocalWeather>();
        foreach (var cell in graph.VoronoiCells)
        {
            if (cell.isOcean) continue;

            cell.weather = new LocalWeather(cell);
            localWeatherList.Add(cell.weather);
        }
        localWeather = localWeatherList.ToArray();
    }
    private void ComputeWeather()
    {
        foreach (var weather in localWeather)
        {
            weather.ComputeWeather();
        }
    }
}

public class LocalWeather
{
    VoronoiCell cell;
    LocalClimate climate;
    Double timeStamp;
    Double nextUpdate = 0.0;
    public float updateInterval = 60.0f; // 
    float water;

    public LocalWeather(VoronoiCell cell)
    {
        this.cell = cell;
        water = 0.0f;
    }

    public void ComputeWeather()
    {
        //ComputePrecipitation();
    }
    private void ComputePrecipitation()
    {
        float precipitationRatePerSecond = climate.PrecipitationDistribution[World.CurrentDate.Month] *
            cell.biome.precipitation / (12 * 33.44f * 24 * 3600) * updateInterval;   
        
        float precipitationRateInInterval = precipitationRatePerSecond * updateInterval;

    }
    private void ComputeEvapotranspiration()
    {   
        
    }

}
