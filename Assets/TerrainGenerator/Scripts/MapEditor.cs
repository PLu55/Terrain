using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using DelaunatorSharp;
using DelaunatorSharp.Unity;
using DelaunatorSharp.Unity.Extensions;
using Unity.VisualScripting;

public class MapEditor : MonoBehaviour
{
    public float mapWidth = 60000.0f;
    public  float mapHeight = 60000.0f;
    public  float pointMinDistance = 10000.0f;
    public  float mapScaleFactor = 0.1f;
    public  float seeLevelTemprature = 24.0f;
    public  float precipitation = 4000.0f;
    public float CoastlineFrequency = 3.0f;
    public float CoastlineThreshold = 0.8f;
    public int CoastlineMaxGeneration= 5;
    public float CoastlineRandomFactor= 0.5f;
    public float CoastlineOffsetX = 0.0f;
    public float CoastlineOffsetY = 0.0f;
    public float ElevationScale = 500.0f;
    public float ElevationFrequency= 3.0f;
    public float ElevationXOffset= 0.0f;
    public float ElevationYOffset= 0.0f;
    public float ElevationRandomFactor= 0.5f;
    [SerializeField] GameObject voronoiVertexPrefab;
    [SerializeField] GameObject voronoiCenterPrefab;
    [SerializeField] Material voronoiEdgeMaterial;
    [SerializeField] Color vEdgeColor = Color.white;
    [SerializeField] Color dEdgeColor = Color.black;
    public float voronoiEdgeWidth = .01f;
    public float delaunayEdgeWidth = .01f;
    public float fontSize = 8;
    public bool drawVoronoiCells = true;
    public bool drawVoronoiVertices = true;
    public bool drawEdges = true;
    public bool drawVoronoiEdges = true;
    public bool drawDelaunayEdges = true;
    public bool showCellDecoration = true;
    public bool createRivers = true;
    public bool applyElevation = true;

    // first material is background and not part of a submesh
    public List<Material> mapMaterials;

    private Graph graph;
    private GraphBuilder graphBuilder;
    private MapBuilder mapBuilder;
    private Transform VertexContainer;
    private Transform CellContainer;
    private Transform EdgeContainer;
    public Transform RiverContainer;

    private float oldCoastlineScale;
    private float oldCoastlineThreshold;
    private int oldCoastlineMaxGeneration;
    private float oldCoastlineRandomFactor;
    private float oldCoastlineOffsetX;
    private float oldCoastlineOffsetY;
    private float oldElevationScale;
    private float oldSeeLevelTemprature;
    private float oldPrecipitation;
    private bool oldCreateRivers;

    private float changeTimer;
    private float changeDuration = 1.0f;
    World world;

    void Start()
    {
        CreateContainers();
        CheckForMapUpdate(); // Just to reset the old values
        changeTimer = float.MinValue;
        world = FindFirstObjectByType<World>(FindObjectsInactive.Include);
        Debug.Assert(world != null, "No world object was found!");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && graph != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            // Check if the ray intersects with a collider
            if (Physics.Raycast(ray, out hit))
            {
                // Get the point of intersection on the collider
                Vector3 hitPoint = hit.point;

                // Print the hit point to the console
                Debug.Log("Hit Point: " + hitPoint);
                VoronoiCell cell = graph.FindCell(hitPoint);
                if (cell != null)
                {
                    Debug.LogFormat("Cell: {0}",cell); 
                    Debug.Log(cell.Info()); 
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Clear();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            world.gameObject.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            ClearMap();
            ClearGraph();
            if (graph != null)
                graph = null;
            if (CellContainer == null)
                CreateContainers();

            CreateGraph();
            DrawGraph();
            CreateMap();
            changeTimer = 0.0f;
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            //ClearMap();
            CreateMap();
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            List<List<VoronoiCell>> depressions = mapBuilder.FindDepressions();
            Debug.LogFormat("Found {0}/{1} depressions seeds", depressions.Count, graph.VoronoiCells.Length);
            int cnt = 0;
            List<VoronoiCell> largestDepression = null;
            foreach (var depression in depressions)
            {
                if (depression.Count() > 3)
                {
                    Debug.LogFormat("Depression around cell: {0}", depression[0]);
                    foreach (var cell in depression)
                    {
                       Debug.LogFormat("   depression member cell: {0}", cell); 
                    }
                }

                if (depression.Count() > cnt)
                {
                    cnt = depression.Count();
                    largestDepression = depression;
                }
            }
            Debug.LogFormat("   largest depression: {0}", largestDepression.Count());
            CreateMap();
        }

        CheckForMapUpdate();

        if (changeTimer > float.MinValue)
        {
            changeTimer -= Time.deltaTime;
            if (changeTimer <= 0.0f)
            {
                CreateMap();
                changeTimer = float.MinValue;
            }
        }

    }

    private bool CheckForMapUpdate()
    {
        if (oldCoastlineScale != CoastlineFrequency ||
            oldCoastlineThreshold != CoastlineThreshold ||
            oldCoastlineMaxGeneration != CoastlineMaxGeneration ||
            oldCoastlineRandomFactor != CoastlineRandomFactor ||
            oldCoastlineOffsetX != CoastlineOffsetX ||
            oldCoastlineOffsetY != CoastlineOffsetY ||
            oldElevationScale != ElevationScale ||
            oldSeeLevelTemprature != seeLevelTemprature ||
            oldPrecipitation != precipitation ||
            oldCreateRivers != createRivers)
        {
            oldCoastlineScale = CoastlineFrequency;
            oldCoastlineThreshold = CoastlineThreshold;
            oldCoastlineMaxGeneration = CoastlineMaxGeneration;
            oldCoastlineRandomFactor = CoastlineRandomFactor;
            oldCoastlineOffsetX = CoastlineOffsetX;
            oldCoastlineOffsetY = CoastlineOffsetY;
            oldElevationScale = ElevationScale;
            oldSeeLevelTemprature = seeLevelTemprature;
            oldPrecipitation = precipitation;
            oldCreateRivers = createRivers;
            changeTimer = changeDuration;
            return true;
        }
        return false;
    }
    void CreateContainers()
    {
        CreateVertexContainer();
        CreateCellContainer();
        CreateEdgeContainer();
    }
    void CreateMap()
    {
        ClearMap();
        CreateMesh();
        DecorateCells();
        world.gameObject.SetActive(true);
    }
    private void CreateGraph()
    {
        graphBuilder = new GraphBuilder(this);
        graphBuilder.Build();
        graph = graphBuilder.GetGraph();
    }
    void CreateMesh()
    {
        Debug.Log("CreateMesh");
        Debug.Assert(graphBuilder != null, "Graph is null, can't create a new mesh");
        Func<float, float> elevation = distance => distance;
    
        mapBuilder = new MapBuilder(this, graph);
        mapBuilder.Build();
    }
    void Clear()
    {
        ClearMap();
        ClearGraph();
    }
    void ClearCells()
    {
        ClearCellDecorators();
        if (graph != null && graph.VoronoiCells != null)
        {
            foreach (var cell in graph.VoronoiCells)
            {
                cell.Clear();
            }
        }
    }
    private void ClearGraph()
    {
        world.gameObject.SetActive(false);
        if (CellContainer != null)
        {
            Destroy(CellContainer.gameObject);
            CellContainer = null;
        }
        if (VertexContainer != null)
        {
            Destroy(VertexContainer.gameObject);
            VertexContainer = null;
        }
        if (EdgeContainer != null)
        {
            Destroy(EdgeContainer.gameObject);
            EdgeContainer = null;
        }
        graph = null;
    }
    void ClearCellDecorators()
    {
        return; // TODO: not working, fix it!
        foreach (Transform child in CellContainer.transform)
        {
            foreach (Transform decorator in child) 
            {
                Destroy(decorator);  
            }    
        }
    }
    private void ClearMap()
    {
        world.gameObject.SetActive(false);
        CellType.CellTypes.Clear();
        ClearCells();
        if (graph != null && graph.meshObject != null)
        {
            GameObject meshObject = graph.meshObject;
            graph.meshObject = null;
            Destroy(meshObject.gameObject);
        }
    }
    void DecorateCells()
    {
        Debug.LogFormat("DecorateCells show: {0} draw: {1}", showCellDecoration, drawVoronoiCells);

        if (showCellDecoration && drawVoronoiCells)
        {
            foreach (var cell in graph.VoronoiCells)
            {
                Debug.AssertFormat(cell.gameObject != null,"No gameObject in cell: {0}", cell);
                if (cell.gameObject != null)
                {
                    AttachLabel(cell.gameObject, cell.ToString(), new Vector3(0.0f, 0.5f, 0.0f));
                    //AttachLabel(cell.gameObject, cell.distanceFromCoast.ToString("F3"), new Vector3(-1.0f, 0.0f, 0.0f));
                    // AttachLabel(cell.gameObject, (cell.distanceFromCoast/graphBuilder.maxDistanceFromCoast).ToString("F3"), new Vector3(-1.0f, 0.0f, 0.0f));
                    if (cell.biome != null){

                        AttachLabel(cell.gameObject, cell.biome.ToStringShort(), new Vector3(0.0f, -0.5f, 0.0f));
                        AttachLabel(cell.gameObject, cell.biome.biotemprature.ToString("F1") + ", " + cell.biome.precipitation.ToString("F1"), new Vector3(0.0f, -1.0f, 0.0f));
                    } 
                    AttachLabel(cell.gameObject, cell.elevation.ToString("F3"), new Vector3(-1.0f, 0.0f, 0.0f));
                    //AttachLabel(cell.gameObject, graphBuilder.RelativeDistanceFromCenterSquared2D(graph.DelaunayVertices[cell.Index]).ToString("F3"), new Vector3(0.0f, -0.5f, 0.0f));
                    //AttachLabel(cell.gameObject, cell.isCoast.ToString(), new Vector3(0.6f, 0.0f, 0.0f));
                    //AttachLabel(cell.gameObject, cell.Vertices.Count.ToString(), new Vector3(-0.6f, 0.0f, 0.0f));
                }
            }
        }
    }
    private void DrawGraph()
    {
        DrawEdges();
        DrawVoronoiCells();
        DrawVoronoiVertices();
    }
    private void DrawVoronoiCells()
    {
        float scale = pointMinDistance * mapScaleFactor;
        if (!(drawVoronoiCells && showCellDecoration)) return;
        Debug.Log("DrawVoronoiCells");
        CreateCellContainer();

        foreach (var cell in graph.VoronoiCells)
        {
            var pointGameObject = Instantiate(voronoiCenterPrefab, CellContainer);
            pointGameObject.transform.SetPositionAndRotation(graph.DelaunayVertices[cell.Index], Quaternion.identity);
            pointGameObject.transform.localScale *=  scale;
            pointGameObject.name = cell.ToString();
            cell.gameObject = pointGameObject;
            //Debug.LogFormat("   cell.gameObject: {0} in cell: {1}", cell.gameObject, cell);
            //AttachLabel(pointGameObject, pointGameObject.name);
        }
    }
    private void DrawEdges()
    {
        if (!drawEdges) return;

        CreateEdgeContainer();
        float scale = pointMinDistance * mapScaleFactor;

        foreach (var e in graph.VoronoiEdges)
        {
            if (drawVoronoiEdges && e == null) continue; 

            Vector3[] points = { graph.CellVertices[e.V0.Index],  graph.CellVertices[e.V1.Index]};
            CreateLine(EdgeContainer, $"Voronoi Edge", points, vEdgeColor, voronoiEdgeWidth * scale , 2);

            if (drawDelaunayEdges && e.D0 != null && e.D1 != null)
            {
                Vector3[] dPoints = { graph.DelaunayVertices[e.D0.Index],  graph.DelaunayVertices[e.D1.Index]};
                CreateLine(EdgeContainer, $"Delaunay Edge", dPoints, dEdgeColor, delaunayEdgeWidth * scale, 1);
            }
        }  
    }

    private void DrawVoronoiVertices()
    {
        if (!drawVoronoiVertices) return;

        CreateVertexContainer();

        foreach (var v in graph.VoronoiVertices)
        {
            var pointGameObject = Instantiate(voronoiVertexPrefab, VertexContainer); 
            pointGameObject.transform.SetPositionAndRotation(graph.CellVertices[v.Index], Quaternion.identity);
            pointGameObject.name = v.ToString();
            AttachLabel(pointGameObject, pointGameObject.name);
        }
    }
    private void AttachLabel(GameObject parent, String text, Vector3? offset = null)
    {
        float scale = pointMinDistance * mapScaleFactor;
        if (offset == null) offset = new Vector3(0, 0.5f, 0);
        offset += new Vector3(0, 0, -0.1f);
        GameObject textObject = new GameObject("AttachedText");
        textObject.transform.SetParent(parent.transform, false);
        textObject.transform.localPosition = (Vector3) offset;
        textObject.transform.localScale = new Vector3(0.4f, 0.4f, 1);
        TextMeshPro textComponent = textObject.AddComponent<TextMeshPro>();
        RectTransform rectTransform = textComponent.GetComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(scale * 0.1f, scale * 0.01f);
        textComponent.text = text;
        textComponent.fontSize = fontSize;
        textComponent.color = Color.black;
        textComponent.alignment = TextAlignmentOptions.Center;
        textComponent.sortingOrder = 3;
    }
    // TODO: this should be placed someware else
    public void CreateLine(Transform container, string name, Vector3[] points, Color color, float width, int order = 1)
    {
        var lineGameObject = new GameObject(name);
        lineGameObject.transform.parent = container;
        var lineRenderer = lineGameObject.AddComponent<LineRenderer>();
        lineRenderer.positionCount = points.Length;
        lineRenderer.SetPositions(points);

        lineRenderer.material = voronoiEdgeMaterial ?? new Material(Shader.Find("Standard"));
        lineRenderer.startColor = color;
        lineRenderer.endColor = color;
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;
        lineRenderer.sortingOrder = order;
    }
    private void CreateVertexContainer()
    {
        if (VertexContainer != null)
        {
            Destroy(VertexContainer.gameObject);
        }

        VertexContainer = new GameObject(nameof(VertexContainer)).transform;
    }

    private void CreateCellContainer()
    {
        if (CellContainer != null)
        {
            Destroy(CellContainer.gameObject);
        }

        CellContainer = new GameObject(nameof(CellContainer)).transform;
    }
    private void CreateEdgeContainer()
    {
        if (EdgeContainer != null)
        {
            Destroy(EdgeContainer.gameObject);
        }

        EdgeContainer = new GameObject(nameof(EdgeContainer)).transform;
    }
    public void CreateRiverContainer()
    {
        Debug.Log("CreateRiverContainer: {0}", RiverContainer);
        if (RiverContainer != null)
        {
            GameObject.Destroy(RiverContainer.gameObject);
            Debug.Log("Destroy RiverContainer");
        }

        RiverContainer = new GameObject(nameof(RiverContainer)).transform;
    }
}
