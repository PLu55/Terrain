using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{
    public static double CurrentTime { get; private set; }
    public static Date CurrentDate { get; private set;}
    public double updateInterval = 1;
    private double nextDateUpdate;
    public Weather weather;
    

    void Start()
    {
        nextDateUpdate = updateInterval;
        CurrentTime = 0.0;
        weather = FindFirstObjectByType<Weather>(FindObjectsInactive.Include);
        Debug.Assert(weather != null, "No weather object was found in the scene!");
        weather.gameObject.SetActive(true);
    }

    void Update()
    {
        if (nextDateUpdate <= Time.timeAsDouble)
        {
            CurrentTime = Time.timeAsDouble;
            CurrentDate = new Date((long)CurrentTime);
            nextDateUpdate += updateInterval;
            
        }
    }
}