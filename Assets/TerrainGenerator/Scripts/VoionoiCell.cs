using System.Collections.Generic;
using UnityEngine;

public class CellType
{
    static public Dictionary<string,CellType> CellTypes { get; } = new Dictionary<string, CellType>();      
    public string Name { get; }
    public int MaterialIndex { get; }
    static public bool HasCellType(string name)
    {
        return  CellTypes.ContainsKey(name);
    }
    static public CellType GetType(string name)
    {   
        return CellTypes[name];
    }
    static public CellType CreateCellType(string name, int materialIndex)
    {
        CellType type = new CellType(name, materialIndex);
        CellTypes.Add(name, type);
        return type;
    }
    override public string ToString()
    {
        return string.Format("CellType({0})", Name);
    }
    CellType(string name, int material)
    {
        Name = name;
        MaterialIndex = material;
    } 
}

public class VoronoiCellElevationComparer : IComparer<VoronoiCell>
{
    public int Compare(VoronoiCell x, VoronoiCell y)
    {
        // Compare based on the Value property
        return x.elevation.CompareTo(y.elevation);
    }
}
// Represents a Voronoi cell
public class VoronoiCell 
{
    // Graph properties
    public GameObject gameObject; // This is the center dot object
    public int Index { get; } // index into VoronoiCell array;
    public int firstTriangle;
    public int numOfTriangles;
    public List<VoronoiCell> Neighbors { get; } 
    public List<VoronoiEdge> Borders { get; } 
    public List<VoronoiVertex> Vertices { get; } 
    // Map properties

    public CellType type;
    public bool isWater;
    public bool isOcean;
    public bool isCoast;
    public bool isBorder;
    public float elevation;
    public float moisture;
    public Biome biome;
    public LocalWeather weather;
    public float distanceFromCoast;
    public float distanceFromCenter;

    public VoronoiCell(int index)
    {
        Index = index;
        Neighbors = new List<VoronoiCell>();
        Borders = new List<VoronoiEdge>();
        Vertices = new List<VoronoiVertex>();
    }

    public void Clear()
    {
        type = null;
        isWater = false;
        isOcean = false;
        isCoast = false;
        elevation = 0.0f;
        moisture = 0.0f;
    }
    override public string ToString()
    {
        return string.Format("C{0}", Index);
    }
    public string Info()
    {
        string cellInfo = string.Empty;
        if (biome != null) 
        { 
            cellInfo = biome.ToStringShort();
        }
        else if (type != null)
        {
            cellInfo = type.Name;
        }
        string what = isOcean ? "isOcean ," : "";
        what += isWater ? "isWater ," : "";
        what += isCoast ? "isCoast ," : "";
        what += isBorder ? "isBorder ," : "";

        if (isBorder)
        {
          what += string.Format("nEdges: {0} nVerts: {1}", Borders.Count, Vertices.Count);  
        }
        string str = string.Format("Cell: {0} {1}  elevation: {2} distance from coast {3} {4}",
            this, cellInfo, elevation, distanceFromCoast, what);
        
        return str;
    }

    int[] GetPolygon()
    {
        int[] vs = new int[Vertices.Count];
        for (int i = 0; i < vs.Length; i++)
        {
            vs[i] = Vertices[i].Index;
        }
        return vs;
    }

    public bool IsPointInCell2D(Vector2 point)
    {
        Vector3[] vertices = Graph.TheGraph.CellVertices;
        //Debug.LogFormat("IsPointInCell2D({0})", point);    
        int[] vix = GetPolygon();
        bool inside = false;
        int j = vix.Length - 1;
        //Debug.LogFormat("    polygon has {0} vertices", vix.Length); 

        for (int i = 0; i < vix.Length; i++)
        {
            if ((vertices[vix[i]].y < point.y && vertices[vix[j]].y >= point.y ||
                 vertices[vix[j]].y < point.y && vertices[vix[i]].y >= point.y) &&
                (vertices[vix[i]].x <= point.x || vertices[vix[j]].x <= point.x))
            {
                if (vertices[vix[i]].x + (point.y - vertices[vix[i]].y) / (vertices[vix[j]].y - vertices[vix[i]].y) * (vertices[vix[j]].x - vertices[vix[i]].x) < point.x)
                {
                    inside = !inside;
                }
            }
            j = i;
        }
        return inside;
    }
    public int[] GetVertexIndices()
    {
        int[] vertices = new int[Vertices.Count];
        for (int i = 0; i < Vertices.Count; i++)
        {
            vertices[i] = Vertices[i].Index;
        }
        return vertices;
    }
    public Vector3[] GetVertexVectors()
    {
        Vector3[] vertices = new Vector3[Vertices.Count];
        for (int i = 0; i < Vertices.Count; i++)
        {
            vertices[i] = Vertices[i].GetVertex();
        }
        return vertices;
    }
    public Vector3 GetCenter()
    {
        return Graph.TheGraph.DelaunayVertices[Index];
    } 
}
