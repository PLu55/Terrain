using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DelaunatorSharp;
using DelaunatorSharp.Unity;
using DelaunatorSharp.Unity.Extensions;
using TMPro;
using System;
public class Tester : MonoBehaviour
{
    [SerializeField] GameObject voronoiVertexPrefab;
    [SerializeField] GameObject voronoiCenterPrefab;
    [SerializeField] Material voronoiEdgeMaterial;
    [SerializeField] Color vEdgeColor = Color.white;
    [SerializeField] Color dEdgeColor = Color.black;
    [SerializeField] float voronoiEdgeWidth = .01f;
    [SerializeField] float delaunayEdgeWidth = .01f;

    [SerializeField]  float fontSize = 6;

    [SerializeField] List<Material> mapMaterials;
    [SerializeField] bool tests = true;
    [SerializeField] bool testVoronoiCells;
    [SerializeField] bool testVoronoiVertices;
    [SerializeField] bool testEdges;
    [SerializeField] bool checkTriangulation;
    [SerializeField] bool drawVoronoiCells;
    [SerializeField] bool drawVoronoiVertices;
    [SerializeField] bool drawEdges;
    [SerializeField] bool drawVoronoiEdges;
    [SerializeField] bool drawDelaunayEdges;

    Delaunator delaunator;
    public Graph graph;
    
    // Start is called before the first frame update

    private Transform TPointsContainer;
    private Transform CellContainer;
    private Transform EdgeContainer;

    public void Start()
    {

    }
    public void Update()
    {

    }
    public void Tests(Delaunator delaunator_)
    {
        Debug.Log("Running tests!");

        delaunator = delaunator_;
 
        // This is fucked!
        //GraphBuilder graphBuilder = new GraphBuilder(null, delaunator, mapMaterials);
        
        //graph = graphBuilder.Graph;
        Debug.Assert(graph != null, "No graph created!");
        Debug.Assert(graph.DelaunayVertices.Length == delaunator.Points.Length, "Number of DelaunayVertices is wrong!");

        graph.mapMaterials = mapMaterials;

        CreateTPointsContainer();
        CreateCellContainer();
        CreateEdgeContainer();

        if (testVoronoiCells) TestVoronoiCells();
        if (drawVoronoiCells) DrawVoronoiCells();
        if (testVoronoiVertices) TestVoronoiVertices();
        if (drawVoronoiVertices) DrawVoronoiVertices();
        if (testEdges) TestEdges();
        if (drawEdges) DrawEdges();
        if (checkTriangulation) CheckTriangulation();


        //graph.VoronoiEdges; 
        //graph.DelaunayVertices;
        //graph.VoronoiVertices;
        //graph = new Graph();
        // check how the vertices are distributed around each cell centroid!
    }

    public void DrawVoronoiCells()
    {
        foreach (var cell in graph.VoronoiCells)
        {
            var pointGameObject = Instantiate(voronoiCenterPrefab, CellContainer);
            pointGameObject.transform.SetPositionAndRotation(graph.DelaunayVertices[cell.Index], Quaternion.identity);
            pointGameObject.name = cell.ToString();
            AttachLabel(pointGameObject, pointGameObject.name);
        }
    }
    public void TestVoronoiCells()
    {
        int n = delaunator.Points.Length;
 
        if (graph.VoronoiCells.Length != n)
            Debug.AssertFormat(graph.VoronoiCells.Length == delaunator.Points.Length,
             "Number of VoronoiCells is wrong {0} expected {1}", 
            graph.VoronoiCells.Length, delaunator.Points.Length);
        
        foreach (var cell in graph.VoronoiCells)
        {
            Debug.AssertFormat(cell.Vertices.Count == cell.Borders.Count,
             "Number of vertices is not equal to number of borders in cell: {0}",
                cell.ToString());            
            Debug.AssertFormat(cell.Neighbors.Count <= cell.Borders.Count,
             "Number of neighbors is not less of equal to number of borders in cell: {0}",
                cell.ToString());
            Debug.LogFormat("Cell: {0} verts: {1} boarders: {2}, Neighbors: {3}", 
            cell.ToString(), cell.Vertices.Count, cell.Borders.Count, cell.Neighbors.Count);
            CheckDuplicateVerticesInCell(cell);
        }
    }
   private void DrawVoronoiVertices()
   {
        foreach (var v in graph.VoronoiVertices)
        {
            var pointGameObject = Instantiate(voronoiVertexPrefab, TPointsContainer); 
            pointGameObject.transform.SetPositionAndRotation(graph.CellVertices[v.Index], Quaternion.identity);
            pointGameObject.name = v.ToString();
            AttachLabel(pointGameObject, pointGameObject.name);
        }
   }
    private void TestVoronoiVertices()
    {
        Debug.LogFormat("Number of VoronoiVertices: {0}", graph.VoronoiVertices.Length);

    }
    private void DrawEdges()
    {
        foreach (var e in graph.VoronoiEdges)
        {
            if (drawVoronoiEdges && e == null) continue; 

            Vector3[] points = { graph.CellVertices[e.V0.Index],  graph.CellVertices[e.V1.Index]};
            CreateLine(EdgeContainer, $"Voronoi Edge", points, vEdgeColor, voronoiEdgeWidth, 2);

            if (drawDelaunayEdges && e.D0 != null && e.D1 != null)
            {
                Vector3[] dPoints = { graph.DelaunayVertices[e.D0.Index],  graph.DelaunayVertices[e.D1.Index]};
                CreateLine(EdgeContainer, $"Delaunay Edge", dPoints, dEdgeColor, delaunayEdgeWidth, 1);
            }
        }  
    }
    private void TestEdges()
    {
        Debug.LogFormat("Number of VoronoiEdges: {0}", graph.VoronoiEdges.Length);
        foreach (var e in graph.VoronoiEdges)
        {
            if (e == null)
                Debug.LogFormat("Not a valid Edge");
            
            Debug.LogFormat("Edge ends vertices: {0} {1}", e.V0 == null, e.V1 == null);
            Debug.LogFormat("Edge ends vertices: {0} {1}", e.V0, e.V1);
            Debug.LogFormat("Edge ends indices: {0} {1}", e.V0.Index, e.V1.Index);

            Vector3[] points = { graph.CellVertices[e.V0.Index],  graph.CellVertices[e.V1.Index]};
            Debug.LogFormat("Len: {0}", points.Length);
            Debug.LogFormat("Edge ends: {0} {1}", points[0], points[1]);
            /*
            if (e.D0 != null && e.D1 != null)
            {
                Vector3[] dPoints = { graph.DelaunayVertices[e.D0.Center],  graph.DelaunayVertices[e.D1.Center]};
            }
            */
        }
    }
    public bool ArePointsClockwise(List<Vector3> points)
    {
        // Calculate centroid of the points
        Vector3 centroid = CalculateCentroid(points);

        // Sort points based on angle with respect to centroid
        points.Sort((p1, p2) => {
            float angle1 = Mathf.Atan2(p1.y - centroid.y, p1.x - centroid.x);
            float angle2 = Mathf.Atan2(p2.y - centroid.y, p2.x - centroid.x);
            return angle1.CompareTo(angle2);
        });

        // Check if points are arranged clockwise
        bool clockwise = true;
        for (int i = 0; i < points.Count; i++)
        {
            Vector3 p1 = points[i];
            Vector3 p2 = points[(i + 1) % points.Count];
            Vector3 p3 = points[(i + 2) % points.Count];

            float crossProduct = CrossProduct(p1, p2, p3);
            if (crossProduct < 0)
            {
                clockwise = false;
                break;
            }
        }

        return clockwise;
    }
        // Calculate centroid of a list of points
    private Vector3 CalculateCentroid(List<Vector3> points)
    {
        Vector3 centroid = Vector3.zero;
        foreach (Vector3 point in points)
        {
            centroid += point;
        }
        centroid /= points.Count;
        return centroid;
    }

    // Calculate cross product of three points
    private float CrossProduct(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        return ((p2.x - p1.x) * (p3.y - p1.y)) - ((p3.x - p1.x) * (p2.y - p1.y));
    }

    private void CreateTPointsContainer()
    {
        if (TPointsContainer != null)
        {
            Destroy(TPointsContainer.gameObject);
        }

        TPointsContainer = new GameObject(nameof(TPointsContainer)).transform;
    }

    private void CreateCellContainer()
    {
        if (CellContainer != null)
        {
            Destroy(CellContainer.gameObject);
        }

        CellContainer = new GameObject(nameof(CellContainer)).transform;
    }
    private void CreateEdgeContainer()
    {
        if (EdgeContainer != null)
        {
            Destroy(EdgeContainer.gameObject);
        }

        EdgeContainer = new GameObject(nameof(EdgeContainer)).transform;
    }

    private void CreateLine(Transform container, string name, Vector3[] points, Color color, float width, int order = 1)
    {
        var lineGameObject = new GameObject(name);
        lineGameObject.transform.parent = container;
        var lineRenderer = lineGameObject.AddComponent<LineRenderer>();

        lineRenderer.SetPositions(points);

        lineRenderer.material = voronoiEdgeMaterial ?? new Material(Shader.Find("Standard"));
        lineRenderer.startColor = color;
        lineRenderer.endColor = color;
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;
        lineRenderer.sortingOrder = order;
    }

    private void AttachLabel(GameObject parent, String text)
    {
        GameObject textObject = new GameObject("AttachedText");
        textObject.transform.SetParent(parent.transform, false);
        textObject.transform.localPosition = new Vector3(0, 0.5f, 0);
        textObject.transform.localScale = new Vector3(0.4f, 0.4f, 1);
        TextMeshPro textComponent = textObject.AddComponent<TextMeshPro>();
        textComponent.text = text;
        textComponent.fontSize = fontSize;
        textComponent.color = Color.black;
        textComponent.alignment = TextAlignmentOptions.Center;
        textComponent.sortingOrder = 3;

    }
    void CheckDuplicateVerticesInCell(VoronoiCell cell)
    {
        HashSet<VoronoiVertex> seenV = new HashSet<VoronoiVertex>();
        HashSet<int> seenI = new HashSet<int>();

        foreach (var vertex in cell.Vertices)
        {
            if (seenV.Contains(vertex))
                Debug.LogErrorFormat("Vertex {1} occures multiple of times in cell: {0}", cell.ToString(), vertex.ToString());
  
            
            if(seenI.Contains(vertex.Index))
                Debug.LogErrorFormat("Vertex {1} with the same index occures in cell: {0}", cell.ToString(), vertex.ToString());
   
            seenV.Add(vertex);
            seenI.Add(vertex.Index);
        }

    }
    void CheckTriangulation()
    {
        HashSet<int> vertexIndices = new HashSet<int>();

        foreach (var cell in graph.VoronoiCells)
        {
            vertexIndices.Clear();
            
            foreach (var v in cell.Vertices)
            {
                vertexIndices.Add(v.Index);
            }
            int n = cell.firstTriangle + cell.numOfTriangles;
            for (int i = cell.firstTriangle; i < n; i++)
            {
                int vix = graph.triangles[i];
                Debug.AssertFormat(vertexIndices.Contains(vix),
                    "Cell {0} has a triangle index {1} that is not s vertex of the cell",
                    cell, vix);
            }
        } 
    }

}
