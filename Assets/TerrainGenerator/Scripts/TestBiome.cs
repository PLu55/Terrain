using System.Collections.Generic;
using UnityEngine;

public class BiomeTester
{
    public static bool TestBiome()
    {
        return TestBiomCoding();
    }
    public static bool TestBiomCoding()
    {
        int i = Biome.EncodeBiomeTypes(BiomeRegion.Boreal, BiomeType.Woodland, BiomeSubType.Wet);
        int[] bs = Biome.DecodeBiomeTypes(i);
        if (bs[0] != (int)BiomeRegion.Boreal ||
            bs[1] != (int)BiomeType.Woodland ||
            bs[2] != (int)BiomeSubType.Wet)
        {
            Debug.LogError("En/DecodBiome is wrong!");
            return false;
        }

        return true;
    }
}