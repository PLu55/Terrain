using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoronoiEdge
{
    // TODO: consider to add reference to the corners array 
    //       then we can implement a method to get the VoronoiVertex points
    //       alternatively in the VoronoiVertex.
    public VoronoiCell D0 { get; } // One of D0 and D1 might be null but not both
    public VoronoiCell D1 { get; }
    public VoronoiVertex V0 { get; }
    public VoronoiVertex V1 { get; }
    public Point midpoint;
    public float River { get; set; }
    public float sloop; // sloop from V0 towards V1, + is upwards

   public VoronoiEdge(VoronoiCell d0, VoronoiCell d1, VoronoiVertex v0, VoronoiVertex v1)
    {
        D0 = d0;
        D1 = d1;
        V0 = v0;
        V1 = v1;
        River = -1;
    }
    override public string ToString()
    {
        return string.Format("E{0}-{1}", D0 == null ? "x": D0.Index, D1 != null ? "x" : D1.Index);
    }
    public float Sloop()
    {
        Vector3 P0 = V0.GetVertex();
        Vector3 P1 = V1.GetVertex();
        return -(P0.z - P1.z) / Vector3.Distance(P0, P1);
    }
    public bool IsCoastLine()
    {
        return D0 != null && D0.isOcean || D1 != null && D1.isOcean;
    }
}
