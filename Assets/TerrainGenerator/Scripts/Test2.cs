using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DelaunatorSharp;
using Unity.VisualScripting;
using UnityEngine.Rendering;

public class Test2 : MonoBehaviour
{
    public bool testVoronoiCellCount = true;
    public bool testVoronoiCells = true;
    public bool testCellTriangulation = true;
    public bool testEdges = true;
    public bool testVoronoiVertices = true;
    public bool testBiome = true;
    public bool testFinds = true;
    public bool testCellSweep = true;
    public bool testEdgeSweep = true;
    public bool testVertexSweep = true;
    public bool testHeap = true;
    public bool testDate = true;
     
    Graph graph;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            RunTests();
        }
    }
    void RunTests()
    {
        int totalCount = 0;
        int passedCount = 0;
        bool pass = true;
        graph = Graph.TheGraph;

#if NDEF
        if (testVoronoiCellCount)
        {
            pass &= TestVoronoiCellCount(delaunator);
            if (pass) passedCount++;
            totalCount++;
        }
#endif
        if (testVoronoiCells) 
        {   
            pass &=  TestVoronoiCells();
            if (pass) passedCount++;
            totalCount++;
        }        
        
        if (testCellTriangulation) 
        {   
            pass &= TestCellTriangulation();
            if (pass) passedCount++;
            totalCount++;
        }

        if (testEdges)
        {
            pass = pass && TestEdges();
            if (pass) passedCount++;
            totalCount++; 
        }

        if (testVoronoiVertices)
        {
            pass = pass && TestVoronoiVertices();
            if (pass) passedCount++;
            totalCount++; 
        }
        if (testBiome)
        {
            pass = pass && BiomeTester.TestBiome();
            if (pass) passedCount++;
            totalCount++; 
        }
        if (testFinds)
        {   
            pass = pass && TestFinds();
            if (pass) passedCount++;
            totalCount++;
        }         
        if (testCellSweep)
        {   
            pass = pass && TestCellSweep();
            if (pass) passedCount++;
            totalCount++;
        } 
        if (testEdgeSweep)
        {   
            pass = pass && TestEdgeSweep();
            if (pass) passedCount++;
            totalCount++;
        } 
        if (testVertexSweep)
        {   
            pass = pass && TestVertexSweep();
            if (pass) passedCount++;
            totalCount++;
        } 
        if (testHeap)
        {
            pass = pass && TestHeap();
            if (pass) passedCount++;
            totalCount++;
        }
        if (testDate)
        {
            pass = pass && TestDate();
            if (pass) passedCount++;
            totalCount++;
        }
        if (pass) Debug.LogFormat("All tests passed {0}/{1}", passedCount, totalCount);
        else Debug.LogErrorFormat("Some tests failed, passed: {0}/{1}", passedCount, totalCount);
    }
    public bool TestVoronoiCellCount(Delaunator delaunator)
    {
        if (graph.VoronoiCells.Length != delaunator.Points.Length)
        {
            Debug.LogErrorFormat( "Number of VoronoiCells is wrong {0} expected {1}", 
            graph.VoronoiCells.Length, delaunator.Points.Length);
            return false;
        }
        return true;
    }
    public bool TestVoronoiCells()
    {   
        bool pass = true;
        HashSet<VoronoiCell> visited = new HashSet<VoronoiCell>();

        foreach (var cell in graph.VoronoiCells)
        {
            if (visited.Contains(cell))
            {
                pass = false;
                break;
            }
            visited.Add(cell);
        }
 
        if (!pass)
        {
            pass = false;
            Debug.LogErrorFormat("graph.VoronoiCells is suspected to have multiple copies of cells, number of cells visited: {0}", visited.Count);
        }

        visited.Clear();

        foreach (var cell in graph.VoronoiCells)
        {
            
            if (cell.Vertices.Count != cell.Borders.Count)
            {
                Debug.LogErrorFormat("Number of vertices is not equal to number of borders in cell: {0}", cell.ToString());
                pass = false; 
            }

            if (cell.Neighbors.Count > cell.Borders.Count)
            {
                Debug.LogErrorFormat("Number of neighbors is not less of equal to number of borders in cell: {0}", cell.ToString());
                pass = false;
            }

            foreach (var neig in cell.Neighbors)
            {
                if (!visited.Contains(neig))
                {
                    visited.Add(neig);
                }
            }
            pass &= CheckDuplicateVerticesInCell(cell);
        }
        if (visited.Count != graph.VoronoiCells.Length)
        {
            Debug.LogErrorFormat("Not all cells are visited when traversing neigbors, only {0}/{1}", visited.Count, graph.VoronoiCells.Length);
            pass = false; 
        }

        return pass;
    }
    private bool TestCellTriangulation()
    {
        HashSet<int> vertexIndices = new HashSet<int>();
        bool pass = true;

        foreach (var cell in graph.VoronoiCells)
        {
            vertexIndices.Clear();
            
            foreach (var v in cell.Vertices)
            {
                vertexIndices.Add(v.Index);
            }
            int n = cell.firstTriangle + cell.numOfTriangles;
            for (int i = cell.firstTriangle; i < n; i++)
            {
                int vix = graph.triangles[i];
                if (!vertexIndices.Contains(vix))
                {
                    pass = false;
                    Debug.LogErrorFormat("Cell {0} has a triangle index {1} that is not s vertex of the cell", cell, vix);
                }
            }
        }
        Debug.Assert(pass, "CheckTriangulation didn't pass the test");
        return pass;
    }
    public bool TestVoronoiVertices()
    {
        bool pass = true;
        int loopCount = 0;
        int maxLoops = 10000000;
        int thisIndex = 0;
        HashSet<VoronoiVertex> visitedVertices = new HashSet<VoronoiVertex>();
        HashSet<VoronoiCell> visitedCells = new HashSet<VoronoiCell>();
        HashSet<VoronoiEdge> visitedEdges = new HashSet<VoronoiEdge>();
        Queue<VoronoiVertex> frontline  = new Queue<VoronoiVertex>();
        VoronoiVertex vertex = graph.VoronoiVertices[0];

        
        if (graph.CellVertices.Length != graph.VoronoiVertices.Length)
        {
            Debug.LogErrorFormat("CellVertices.Length not equal to VoronoiCells.Length as expected: {0} {1}",
                graph.CellVertices.Length , graph.VoronoiCells.Length);
            return false;
        }

        frontline.Enqueue(vertex);

        do
        {
            if (!frontline.TryDequeue(out vertex))
            {
                Debug.LogErrorFormat("Premature exit, queue is empty, {0} vertices has been visited", visitedVertices.Count);
                pass = false;
                break;
            }

            visitedVertices.Add(vertex);
            thisIndex = vertex.Index;

            if (thisIndex >= graph.VoronoiVertices.Length)
            {                
                Debug.LogErrorFormat("Illegal VoronoiVertex index: {0}", thisIndex);
                pass = false;
                break;
            }


            if (vertex != graph.VoronoiVertices[thisIndex])
            {
                Debug.LogErrorFormat("VoronoiVertex index doen't point to it self {0}", thisIndex);
            }

            foreach (var adjacent in vertex.adjacent)
            {
               if (!visitedVertices.Contains(adjacent))
               {
                    frontline.Enqueue(adjacent);
               }
            }
            foreach (var cell in vertex.touches)
            {
                if (!visitedCells.Contains(cell))
                    visitedCells.Add(cell);
            } 
             foreach (var edge in vertex.protrudes)
            {
                if (!visitedEdges.Contains(edge))
                    visitedEdges.Add(edge);
            } 
            

        } while (visitedVertices.Count < graph.CellVertices.Length && loopCount++ < maxLoops);

        pass = pass && visitedVertices.Count == graph.CellVertices.Length;

        if (visitedEdges.Count != graph.VoronoiEdges.Length)
        {
            Debug.LogErrorFormat("Not all VoronoiEdges where visited, only {0}/{1}", visitedEdges.Count, graph.VoronoiEdges.Length);
            pass = false;
        }
        if (visitedCells.Count != graph.VoronoiCells.Length)
        {
            Debug.LogErrorFormat("Not all VoronoiCells where visited, only {0}/{1}", visitedCells.Count, graph.VoronoiCells.Length);
            pass = false;
        }
          
        if (visitedVertices.Count != graph.CellVertices.Length)
        {
            Debug.LogErrorFormat("TestVoronoiVertices did not pass the test, not all vertices are connected, {0}/{1} vertices where visited after {2} turns", 
            visitedVertices.Count, graph.CellVertices.Length, loopCount);
        }
        else if (!pass) 
        {
            Debug.LogErrorFormat("TestVoronoiVertices did not pass the test");
        }
        return pass;
    }
    private bool TestEdges()
    {
        bool pass = true;

        foreach (var e in graph.VoronoiEdges)
        {
            if (e.V0 == null || e.V1 == null)
            {
                Debug.LogErrorFormat("One or both edge vertices are null: {0} {1}", e.V0 == null, e.V1 == null);
                pass = false;
            } 
            if (e.V0.Index >= graph.CellVertices.Length || e.V1.Index >= graph.CellVertices.Length)
            {
                Debug.LogErrorFormat("One or both edge vertices are not legal indices: {0} {1} vertices.Length: {2}", e.V0.Index, e.V1.Index, graph.CellVertices.Length);
            }
        }
        return pass;
    }
    bool CheckDuplicateVerticesInCell(VoronoiCell cell)
    {
        bool pass = true;

        HashSet<VoronoiVertex> seenV = new HashSet<VoronoiVertex>();
        HashSet<int> seenI = new HashSet<int>();

        foreach (var vertex in cell.Vertices)
        {
            if (seenV.Contains(vertex))
            {
                Debug.LogErrorFormat("Vertex {1} occures multiple of times in cell: {0}", cell.ToString(), vertex.ToString());
                pass = false;
            }
  
            
            if(seenI.Contains(vertex.Index))
            {
                Debug.LogErrorFormat("Vertex {1} with the same index occures in cell: {0}", cell.ToString(), vertex.ToString());
                pass = false;
            }
   
            seenV.Add(vertex);
            seenI.Add(vertex.Index);
        }
        return pass;
    }

    float Vector2SquaredDistance(Vector2 a, Vector2 b)
    {
        return (a-b).sqrMagnitude;
    }
    bool TestFinds()
    {
        VoronoiCell cell = graph.FindCell(graph.Bounds.center);

        if (cell == null)
        {
            Debug.LogErrorFormat("FindCell didn't find the center cell, center: {0}", graph.Bounds.center);

            float minDist = float.MaxValue;
            int ix = -1;
            for(int i = 0; i < graph.points.Length; i++)
            {
                float dist = Vector2SquaredDistance(graph.Bounds.center, graph.points[i]);
                if (dist < minDist)
                {
                    minDist = dist;
                    ix = i;
                }
            }
            if (ix == -1)
            {
                Debug.LogError("Nothing at all was found");
            }

            return false;
        }

        VoronoiEdge edge = graph.FindEdge(graph.Bounds.center);
        if (edge == null)
        {
            Debug.LogError("FindCell didn't find a central edge");
            return false;
        }
               
        VoronoiVertex vertex = graph.FindVertex(graph.Bounds.center);
        if (edge == null)
        {
            Debug.LogError("FindCell didn't find a central vertex");
            return false;
        }
        return true;

    }
    bool TestCellSweep()
    {
        int count = 0;
        HashSet<VoronoiCell> visited = new HashSet<VoronoiCell>();

        visited.Clear();

        foreach (var cell in graph.CellSweep())
        {
            if (count++ > graph.VoronoiCells.Length) break;
            if (visited.Contains(cell))
            {
                Debug.LogErrorFormat("TestCellSweep: cell: {0} already seen", cell);
            }
            visited.Add(cell);
        }

        if (count != graph.VoronoiCells.Length)
        {
            Debug.LogErrorFormat("CellSweep from center, not all cells where visited, only {0}/{1}", count, graph.VoronoiCells.Length);
            return false;
        }
        count = 0;

        foreach (var cell in graph.CellSweep(SweepDirection.FromBoarder))
        {
            count++;
        }

        if (count != graph.VoronoiCells.Length)
        {
            Debug.LogErrorFormat("CellSweep from border, not all cells where visited, only {0}/{1}", count, graph.VoronoiCells.Length);
            return false;
        }
       return true;
    }    
    bool TestEdgeSweep()
    {
        bool pass = true;
        HashSet<VoronoiEdge> visited = new HashSet<VoronoiEdge>();
        int count = 0;
        
        foreach (var edge in graph.EdgeSweep())
        {
            count++;
            if (visited.Contains(edge))
            {
                Debug.LogErrorFormat("EdgeSweep from center, edge already seen");
                pass = false;
            }
            visited.Add(edge);
        }

        if (count != graph.VoronoiEdges.Length)
        {
            Debug.LogErrorFormat("EdgeSweep from center, no all edges visited, only {0}/{1}", count, graph.VoronoiEdges.Length);
            pass = false;
        }
        
        count = 0;
        visited.Clear();

        foreach (var edge in graph.EdgeSweep(SweepDirection.FromBoarder))
        {
            count++;
            if (visited.Contains(edge))
            {
                Debug.LogErrorFormat("EdgeSweep from border, edge already seen");
                pass = false;
            }
            visited.Add(edge);

        }

        if (count != graph.VoronoiEdges.Length)
        {
            Debug.LogErrorFormat("EdgeSweep from border, no all edges visited, only {0}/{1}", count, graph.VoronoiEdges.Length);
            pass = false;
        }
        return pass;
    }
    bool TestVertexSweep()
    {
        bool pass = true;
        HashSet<VoronoiVertex> visited = new HashSet<VoronoiVertex>();
        int count = 0;
        
        foreach (var vertex in graph.VertexSweep())
        {
            count++;
            if (visited.Contains(vertex))
            {
                Debug.LogErrorFormat("VertexSweep from center, edge already seen");
                pass = false;
            }
            visited.Add(vertex);

        }

        if (count != graph.VoronoiVertices.Length)
        {
            Debug.LogErrorFormat("VertexSweep from center, no all vertices visited, only {0}/{1}", count, graph.VoronoiVertices.Length);
           pass = false;
        }

        count = 0;
        visited.Clear();

        foreach (var vertex in graph.VertexSweep(SweepDirection.FromBoarder))
        {
            count++;
            if (visited.Contains(vertex))
            {
                Debug.LogErrorFormat("VertexSweep from border, edge already seen");
                pass = false;
            }
        }

        if (count != graph.VoronoiVertices.Length)
        {
            Debug.LogErrorFormat("VertexSweep from border, no all vertices visited, only {0}/{1}", count, graph.VoronoiVertices.Length);
            pass = false;
        }
        return pass;
    }
    public class FloatComparer : IComparer<float>
    {
        public int Compare(float x, float y)
        {
            return x.CompareTo(y);
        }
    }
    private bool TestHeap()
    {
        Heap<float> heap = new Heap<float>(new FloatComparer());

        float[] xs = {1, 5, 19, 7, 3, 2};
        foreach (float x in xs)
            heap.Enqueue(x);
        
        float min = float.NegativeInfinity;
        while (!heap.IsEmpty())
        {
            float x = heap.Dequeue();
            if (x < min) 
            {
                Debug.LogError("TestHeap failed, lower value then the seen ones returned");
                return false;
            }
            min = x;
        }
        return true;
    }

    private bool TestDate()
    {
        bool pass = true;
        Date.MonthFromSeconds(0); // initialize monthShift

        for (int i = 0; i < 12; i++)
        {
            int d0 = Date.monthShift[i];
            int d1 = i == 11 ? 364 : (Date.monthShift[i+1] - 1);
            //Debug.LogFormat("Date i: {0} d0: {1} d1: {2}", i, d0, d1);
            long sec0 = d0 * Date.secondsPerDay;
            long sec1 = d1 * Date.secondsPerDay;
            int m0 = Date.MonthFromSeconds(sec0);
            int m1 = Date.MonthFromSeconds(sec1);
            //Debug.LogFormat("    sec0: {0} sec1: {1}", sec0, sec1);
            //Debug.LogFormat("    m0: {0} m1: {1}", m0, m1);
            if (m0 != i + 1 || m1 != i + 1)
            {
                Debug.AssertFormat(m0 == i + 1, "MonthFromSeconds is wrong, day0: {0} m0: {1} expected: {2}", d0, m0, i + 1); 
                Debug.AssertFormat(m1 == i + 1, "MonthFromSeconds is wrong, day1: {0} m1: {1} expected: {2}", d1, m1, i + 1); 
                pass = false;
            }
            
            long secs0 = Date.MonthToSeconds(m0);
            long secs1 = Date.MonthToSeconds(m1);
            if (secs0 != sec0 || secs1 != sec0)
            {
                Debug.AssertFormat(secs0 == sec0, "MonthToSeconds is wrong, secs0: {0} m0: {1} expected: {2}", secs0, m0, sec0); 
                Debug.AssertFormat(secs1 == sec0, "MonthToSeconds, secs1: {0} m1: {1} expected: {2}", secs1, m1, sec0); 
                pass = false;
            }
            //Debug.LogFormat("    secs0: {0} secs1: {1}", secs0, secs1);
        }

        //long tx = DateTimeOffset.Now.ToUnixTimeSeconds();
        Date date = new Date(1970, 1, 1, 8, 36, 40);
        //Date date = new Date(3600*24 * 33);
        Debug.LogFormat("date: {0}", date);
        long sec = date.ToSeconds();
        long t = 8 * 3600 + 36 * 60 + 40;
        pass = sec == t;
        Debug.LogFormat("seconds: {0} expected: {1} pass: {2}", sec, t, pass);
        int month = Date.MonthFromSeconds(sec);

        //Debug.LogFormat("seconds: {0}, {1}, {2}", sec, sec/Date.secondsPerYear, month );
        Date date2 = new Date(1983, 12, 15, 8, 36, 40);
        int day = (int)Date.MonthToSeconds(12) / Date.secondsPerDay;
        long sec2 = date2.ToSeconds();
        long expected2 = 440066200;
        Debug.LogFormat("dayOfYear: {0}", day);
        if (sec2 != expected2)
        {
            Debug.LogErrorFormat("Date {0} to seconds {1} is not what is expected {2}", date2, sec2, expected2);
            pass = false;
        }
        Date date3 = new Date(sec2);
        Debug.LogFormat("date3: {0}", date3);

        if (date2 != new Date(sec2))
        {
            Debug.LogError("Date not equal to it's own result of ToSeconds");
            pass = false;
        }
        return pass;
    }
}
