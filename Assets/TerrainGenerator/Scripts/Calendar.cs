using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Calendar : MonoBehaviour
{
    public float rate;
    public float updateInterval = 1;
    private float nextUpdate;
    public float time;
    public float t0;
    private Date dateCache;
    

    void Start()
    {
        nextUpdate = updateInterval;
        time = t0;
    }

    void Update()
    {
        time += Time.deltaTime;
        nextUpdate -= Time.deltaTime;
        if (nextUpdate <= 0)
        {
            nextUpdate = updateInterval;
            dateCache = null;
        }
    }

}
