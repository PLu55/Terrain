using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DelaunatorSharp;
using JetBrains.Annotations;
using Unity.Services.Analytics;
using Unity.Properties;
using DelaunatorSharp.Unity.Extensions;

public class Generator : MonoBehaviour
{
    static private float SquareRootTwo = Mathf.Sqrt(2.0f);

    public const int DefaultPointsPerIteration = 30;
    public List<Vector2> points;
    struct State
    {
        public Vector2?[,] Grid;
        public List<Vector2> ActivePoints, Points;
    }
    struct Settings
    {
        public Vector2 TopLeft, LowerRight, Center;
        public Vector2 Dimensions;
        public float? RejectionSqDistance;
        public float MinimumDistance;
        public float CellSize;
        public int GridWidth, GridHeight;
    }

    public IPoint[] Points { get;  private set; }
    void Start()
    {
        Vector2 topLeft = new Vector2(-10.0f, -10.0f);
        Vector2 lowerRight = new Vector2(10.0f, 10.0f);
        float minimumDistance = 1.0f;
        
        points = SampleRectangle(topLeft, lowerRight, minimumDistance);
        Points = new Point[points.Count];

        float minX = 0, maxX = 0, minY = 0, maxY = 0;
        int i = 0;

        foreach (var pnt in points)
        {
            Points[i] = new Point(pnt.x, pnt.y);

            minX = Mathf.Min(pnt.x, minX);
            maxX = Mathf.Max(pnt.x, maxX);
            minY = Mathf.Min(pnt.y, minY);
            maxY = Mathf.Max(pnt.y, minY);
        }
        Debug.LogFormat("Point[0] = {0}", Points[0]);
        Debug.LogFormat("{0} <= X <= {1}; {2} <= Y <= {3}", minX, maxX, minY, maxY);
        Delaunator delaunator = new Delaunator(points.ToPoints());
        
    }

    public static List<Vector2> SampleCircle(Vector2 center, float radius, float minimumDistance)
    {
        return SampleCircle(center, radius, minimumDistance, DefaultPointsPerIteration);
    }
    public static List<Vector2> SampleCircle(Vector2 center, float radius, float minimumDistance, int pointsPerIteration)
    {
        return Sample(center - new Vector2(radius, radius), center + new Vector2(radius, radius), radius, minimumDistance, pointsPerIteration);
    }

    public static List<Vector2> SampleRectangle(Vector2 topLeft, Vector2 lowerRight, float minimumDistance)
    {
        return SampleRectangle(topLeft, lowerRight, minimumDistance, DefaultPointsPerIteration);
    }
    public static List<Vector2> SampleRectangle(Vector2 topLeft, Vector2 lowerRight, float minimumDistance, int pointsPerIteration)
    {
        return Sample(topLeft, lowerRight, minimumDistance, null, pointsPerIteration);
    }
    static List<Vector2> Sample(Vector2 topLeft, Vector2 lowerRight, 
                    float minimumDistance, float? rejectionDistance,
                    int pointsPerIteration)
    {
        var settings = new Settings
        {
            TopLeft = topLeft,
            LowerRight = lowerRight,
            Dimensions = lowerRight - topLeft,
            Center = (topLeft + lowerRight) / 2,
            CellSize = minimumDistance / SquareRootTwo,
            MinimumDistance = minimumDistance,
            RejectionSqDistance = rejectionDistance == null ? null : rejectionDistance * rejectionDistance
        };

        settings.GridWidth = (int)(settings.Dimensions.x / settings.CellSize) + 1;
        settings.GridHeight = (int)(settings.Dimensions.y / settings.CellSize) + 1;

        var state = new State
        {
            Grid = new Vector2?[settings.GridWidth, settings.GridHeight],
            ActivePoints = new List<Vector2>(),
            Points = new List<Vector2>()
        };

        AddFirstPoint(ref settings, ref state);

        while (state.ActivePoints.Count != 0)
        {
            var listIndex = RandomHelper.Random.Next(state.ActivePoints.Count);

            var point = state.ActivePoints[listIndex];
            var found = false;

            for (var k = 0; k < pointsPerIteration; k++)
                found |= AddNextPoint(point, ref settings, ref state);

            if (!found)
                state.ActivePoints.RemoveAt(listIndex);
        }

        return state.Points;
    }

    static void AddFirstPoint(ref Settings settings, ref State state)
    {
        var added = false;
        while (!added)
        {
            var d = RandomHelper.Random.NextDouble();
            var xr = settings.TopLeft.x + settings.Dimensions.x * d;

            d = RandomHelper.Random.NextDouble();
            var yr = settings.TopLeft.y + settings.Dimensions.y * d;

            var p = new Vector2((float)xr, (float)yr);
            if (settings.RejectionSqDistance != null && DistanceSquared(settings.Center, p) > settings.RejectionSqDistance)
                continue;
            added = true;

            var index = Denormalize(p, settings.TopLeft, settings.CellSize);

            state.Grid[(int)index.x, (int)index.y] = p;

            state.ActivePoints.Add(p);
            state.Points.Add(p);
        }
    }
    static bool AddNextPoint(Vector2 point, ref Settings settings, ref State state)
    {
        var found = false;
        var q = GenerateRandomAround(point, settings.MinimumDistance);

        if (q.x >= settings.TopLeft.x && q.x < settings.LowerRight.x &&
            q.y > settings.TopLeft.y && q.y < settings.LowerRight.y &&
            (settings.RejectionSqDistance == null || DistanceSquared(settings.Center, q) <= settings.RejectionSqDistance))
        {
            var qIndex = Denormalize(q, settings.TopLeft, settings.CellSize);
            var tooClose = false;

            for (var i = (int)Mathf.Max(0, qIndex.x - 2); i < Mathf.Min(settings.GridWidth, qIndex.x + 3) && !tooClose; i++)
                for (var j = (int)Mathf.Max(0, qIndex.y - 2); j < Mathf.Min(settings.GridHeight, qIndex.y + 3) && !tooClose; j++)
                    if (state.Grid[i, j].HasValue && Vector2.Distance(state.Grid[i, j].Value, q) < settings.MinimumDistance)
                        tooClose = true;

            if (!tooClose)
            {
                found = true;
                state.ActivePoints.Add(q);
                state.Points.Add(q);
                state.Grid[(int)qIndex.x, (int)qIndex.y] = q;
            }
        }
        return found;
    }
    static Vector2 Denormalize(Vector2 point, Vector2 origin, double cellSize)
    {
        return new Vector2((int)((point.x - origin.x) / cellSize), (int)((point.y - origin.y) / cellSize));
    }
    static Vector2 GenerateRandomAround(Vector2 center, float minimumDistance)
    {
        var d = RandomHelper.Random.NextDouble();
        var radius = minimumDistance + minimumDistance * d;

        d = RandomHelper.Random.NextDouble();
        var angle = (float)(MathHelper.TwoPi * d);

        var newX = radius * Mathf.Sin(angle);
        var newY = radius * Mathf.Cos(angle);

        return new Vector2((float)(center.x + newX), (float)(center.y + newY));
    }

    static float DistanceSquared(Vector2 a, Vector2 b)
    {
        float cx = a.x - b.x;
        float cy = a.y - b.y;
        return cx * cx + cy * cy;
    } 
 }

public static class RandomHelper
{
    public static readonly System.Random Random = new System.Random();
}

public static class MathHelper
{
    public const float Pi = (float)Mathf.PI;
    public const float HalfPi = (float)(Mathf.PI / 2.0f);
    public const float TwoPi = (float)(Mathf.PI * 2.0f);
}

public class Point : IPoint
{
    public double X { get; set; }
    public double Y { get; set; }

    public Point(float x, float y)
    {
        X = (double) x;
        Y = (double) y;
    }
    public Point(double x, double y)
    {
        X = x;
        Y = y;
    }

    public override string ToString()
    {
        return $"({X}, {Y})";
    }
}