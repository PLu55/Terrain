# Terrain tools

## Generating maps

[Polygonal Map Generation for Games](http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/#edges)

## Delaunator

Delaunator-sharp is a portation of Delaunator to C# and Unity.
Delaunator is a Java implementation of Delaunay triangulation.
https://github.com/mapbox/delaunator?tab=readme-ov-file

Follow the README in Delaunator-sharp to install into a new 3D
project.

There is a demo, it took some work to get it running. Add
DelaunatorPreview to a new sceen, run and enter <return> to create a
result, <space> will clear.

[Delaunator guide](https://mapbox.github.io/delaunator/)

The UniforPoissonDiskSampler generated a list of random ponits in a
circular or rectagular shape.


Delaunator is the main class that creates the Delaunay-Voronoi 

Constructor to construct a new graph from points (Vector2[])
delaunator = new Delaunator(points.ToArray());

Fields:

	// One value per half-edge, containing the point index of where a given half edge starts.
	public int[] Triangles { get; private set; }
	
	// One value per half-edge, containing the opposite half-edge in the adjacent triangle,
	// or -1 if there is no adjacent triangle
	public int[] Halfedges { get; private set; }
	
	// Original Points given to construct the Delaunator
	public IPoint[] Points { get; private set; }
	
	// A list of point indices that traverses the hull of the points.
    public int[] Hull { get; private set; }
	
GetMethods:

	public IEnumerable<ITriangle> GetTriangles()
	public IEnumerable<IEdge> GetEdges()
	public IEnumerable<IEdge> GetVoronoiEdges(Func<int, IPoint> triangleVerticeSelector = null)
	public IEnumerable<IEdge> GetVoronoiEdgesBasedOnCircumCenter()
    public IEnumerable<IEdge> GetVoronoiEdgesBasedOnCentroids()
	public IEnumerable<IVoronoiCell> GetVoronoiCells(Func<int, IPoint> triangleVerticeSelector = null)
	public IEnumerable<IVoronoiCell> GetVoronoiCellsBasedOnCircumcenters()
	public IEnumerable<IVoronoiCell> GetVoronoiCellsBasedOnCentroids()
	public IEnumerable<IEdge> GetHullEdges()

    public IPoint[] GetHullPoints()

    public IPoint[] GetTrianglePoints(int t)
    public IPoint[] GetRellaxedPoints()
	public IEnumerable<IEdge> GetEdgesOfTriangle(int t)
    public static IEnumerable<IEdge> CreateHull(IEnumerable<IPoint> points)
    public IPoint GetTriangleCircumcenter(int t)
	public IPoint GetCentroid(int t)
	public static IPoint GetCircumcenter(IPoint a, IPoint b, IPoint c)
    public static IPoint GetCentroid(IPoint[] points)
	
Iterators:

	public void ForEachTriangle(Action<ITriangle> callback)
    public void ForEachTriangleEdge(Action<IEdge> callback)
    public void ForEachVoronoiEdge(Action<IEdge> callback)
    public void ForEachVoronoiCellBasedOnCentroids(Action<IVoronoiCell> callback)
    public void ForEachVoronoiCellBasedOnCircumcenters(Action<IVoronoiCell> callback)
	public void ForEachVoronoiCell(Action<IVoronoiCell> callback, Func<int, IPoint> triangleVertexSelector = null)
	
	// Methods based on index
	// Returns the half-edges that share a start point with the given half edge, in order.
	public IEnumerable<int> EdgesAroundPoint(int start)
	
	// Returns the three point indices of a given triangle id.
	public IEnumerable<int> PointsOfTriangle(int t)
	
	// Returns the triangle ids adjacent to the given triangle id.
	// Will return up to three values.
	public IEnumerable<int> TrianglesAdjacentToTriangle(int t)
	
Based on index: where e is halfedge and t triangle:

	// e is halfedge index and t triangle index
	public static int NextHalfedge(int e)
	public static int PreviousHalfedge(int e)
	
	// Returns the three half-edges of a given triangle id.
	public static int[] EdgesOfTriangle(int t)
	// Returns the triangle id of a given half-edge.
	public static int TriangleOfEdge(int e)
	


A Mesh can be created from like this (see  DelaunatorPreview.cs)

           var mesh = new Mesh
            {
                vertices = delaunator.Points.ToVectors3(),
                triangles = delaunator.Triangles
            };
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
			

	public interface IEdge
	{
		IPoint P { get; }
		IPoint Q { get; }
		int Index  { get; }
	}
	
	public interface IVoronoiCell
    {
		IPoint[] Points { get; }
		int Index { get; }
	}
	
    public interface IPoint
    {
        double X { get; set; }
        double Y { get; set; }
    }
	
    public interface ITriangle
    {
        IEnumerable<IPoint> Points { get; }
        int Index { get; }
    }


	
	 P.ToVector3D()
	 Q.ToVector3D()


### How to navigate the Mesh

A Mesh has triangles, Vertices, submeshes and some other stuff.
Each submeshes has it's own material.

Note that meshes use 16-bit indexFormat by default, max 65535

A Triangle has a list of indecies to the Vertices.

### Representing the map

Amit Patel Red Blob Games
[Polygonal Map Generation for Games]http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/)
[The code for mapgen2](https://github.com/amitp/mapgen2)

Two Graphs: nodes and edges
DelaunayGraph: triangle           edge
VoronoiGraph:  polygon-corner     edge


### Voronoi polygons

https://www.redblobgames.com/x/1721-voronoi-alternative/

Use the centroid of the Delaunay triagles as vertices. The result is
not a proper Voronoi mesh but a barycentric dual mesh. This has a some
wanted properties, the vertices of the polygons are alway inside the
corosponding Delaunay triagle the edges of the two structures are
properly crossing in a one-to-one relation.

### How to generate the graph

The graph is a stucture based on the VorioniEdge, everything can be
reached from it.

  * Traverse all Delaunay vertex and create a Vorononi cell for each
    except for a vertex which is a member of the hull
  * Traverse all Halfedges

### How to generate a mesh from the Voronoi polygons

Look into private void CreateVoronoi()

To traverse the pointes and get Voronoi polygons from that, skip the
points included in the hull. Project the outer edges of the Voronoi
polygons to the frame.
